const express = require('express');
const http = require("http");
const socketIo = require("socket.io");
const path = require('path');
const fileUpload = require('express-fileupload');
const cors = require('cors');

const bodyParser = require('body-parser');
const auth = require("./auth.js")();

const secret = require("./auth_config.js");
const jwt = require("jsonwebtoken");
const socketioJwt = require('socketio-jwt')

const userRouter = require('./routes/userRoute');
const login = require('./routes/login');
const main = require('./routes/main');
const signUp = require('./routes/signUp');

const app = express();
const server = http.createServer(app);
const io = socketIo(server);
const port = 8000;

const betController = require('./controllers/betController');

// app.listen(port,   function(){
//     console.log("server runs on port " + port + " at " + new Date().getHours() + ":" + new Date().getMinutes() );
// });

app.use(
  cors(),
	bodyParser.json(),
	bodyParser.urlencoded({ extended: false }),
  (req, res, next) => {
		  res.header("Access-Control-Allow-Origin", "*");
		  res.header("Access-Control-Allow-Headers", "*");
		  res.header("Access-Control-Allow-Methods", "*");
		  next();
	},
  auth.initialize(),
  (req, res, next) => {
    console.log(req.url);
    next()
  },
  fileUpload(),
)
app.use('/public', express.static(__dirname + '/public'))

app.use('/', login);
app.use('/', signUp);
app.use('/user', userRouter);
app.use('/main',  main)

function isValid(data) {
  return true;
}

io.on("connection", socket => {
  console.log('client connected');

  socket.on('room', room => {
    if (socket.room) socket.leave(socket.room);
    console.log(`client joined room number ${room}`);
    socket.room = room;
    socket.join(room);
    betController.getAllAuctionBets(socket);
  });

  socket.on('makeBit', (data)=> {
    if (isValid(data)) betController.makeBet(io, socket, data);
  });

  socket.on("disconnect", () => console.log("Client disconnected"));
});

server.listen(port, () => console.log(`Listening on port ${port}`));
