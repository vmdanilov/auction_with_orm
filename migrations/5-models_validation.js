'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * changeColumn "minStep" on table "Auctions"
 * changeColumn "startBid" on table "Auctions"
 * changeColumn "endDate" on table "Auctions"
 * changeColumn "startDate" on table "Auctions"
 * changeColumn "amount" on table "Bets"
 * changeColumn "info" on table "Lots"
 * changeColumn "photo" on table "Lots"
 * changeColumn "name" on table "Lots"
 * changeColumn "name" on table "Lots"
 * changeColumn "password" on table "Users"
 *
 **/

var info = {
    "revision": 5,
    "name": "models_validation",
    "created": "2018-05-24T13:48:19.181Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "changeColumn",
        params: [
            "Auctions",
            "minStep",
            {
                "type": Sequelize.INTEGER,
                "validate": {
                    "notNull": true,
                    "isInt": true
                }
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Auctions",
            "startBid",
            {
                "type": Sequelize.INTEGER,
                "validate": {
                    "notNull": true,
                    "isInt": true
                }
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Auctions",
            "endDate",
            {
                "type": Sequelize.DATE,
                "validate": {
                    "notNull": true
                }
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Auctions",
            "startDate",
            {
                "type": Sequelize.DATE,
                "validate": {
                    "notNull": true
                }
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Bets",
            "amount",
            {
                "type": Sequelize.INTEGER,
                "validate": {
                    "notNull": true,
                    "isInt": true
                }
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Lots",
            "info",
            {
                "type": Sequelize.STRING(1234)
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Lots",
            "photo",
            {
                "type": Sequelize.STRING,
                "validate": {
                    "notNull": true,
                    "notEmpty": true,
                    "isUrl": true
                }
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Lots",
            "name",
            {
                "type": Sequelize.STRING,
                "validate": {
                    "isAlpha": true,
                    "notNull": true,
                    "notEmpty": true
                }
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Lots",
            "name",
            {
                "type": Sequelize.STRING,
                "validate": {
                    "isAlpha": true,
                    "notNull": true,
                    "notEmpty": true
                }
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Users",
            "password",
            {
                "type": Sequelize.STRING,
                "validate": {
                    "is": {}
                },
                "allowNull": false
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
