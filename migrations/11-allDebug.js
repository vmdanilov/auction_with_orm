'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * changeColumn "minStep" on table "Auctions"
 * changeColumn "startBid" on table "Auctions"
 * changeColumn "endDate" on table "Auctions"
 * changeColumn "startDate" on table "Auctions"
 * changeColumn "amount" on table "Bets"
 * changeColumn "password" on table "Users"
 *
 **/

var info = {
    "revision": 11,
    "name": "allDebug",
    "created": "2018-05-24T14:11:26.509Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "changeColumn",
        params: [
            "Auctions",
            "minStep",
            {
                "type": Sequelize.INTEGER,
                "validate": {
                    "isInt": true
                }
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Auctions",
            "startBid",
            {
                "type": Sequelize.INTEGER,
                "validate": {
                    "isInt": true
                }
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Auctions",
            "endDate",
            {
                "type": Sequelize.DATE,
                "validate": {}
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Auctions",
            "startDate",
            {
                "type": Sequelize.DATE,
                "validate": {}
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Bets",
            "amount",
            {
                "type": Sequelize.INTEGER,
                "validate": {
                    "isInt": true
                }
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Users",
            "password",
            {
                "type": Sequelize.STRING,
                "validate": {
                    "is": {}
                },
                "allowNull": false
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
