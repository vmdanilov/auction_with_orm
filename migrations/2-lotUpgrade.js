'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * addColumn "doc" to table "Lots"
 * changeColumn "password" on table "Users"
 *
 **/

var info = {
    "revision": 2,
    "name": "lotUpgrade",
    "created": "2018-05-18T11:41:44.611Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "addColumn",
        params: [
            "Lots",
            "doc",
            {
                "type": Sequelize.STRING
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Users",
            "password",
            {
                "type": Sequelize.STRING,
                "validate": {
                    "is": {}
                },
                "allowNull": false
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
