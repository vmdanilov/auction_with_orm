'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * changeColumn "name" on table "Lots"
 * changeColumn "password" on table "Users"
 *
 **/

var info = {
    "revision": 10,
    "name": "lotDebug4",
    "created": "2018-05-24T14:09:18.616Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "changeColumn",
        params: [
            "Lots",
            "name",
            {
                "type": Sequelize.STRING,
                "validate": {
                    "isAlpha": true,
                    "notEmpty": true
                }
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Users",
            "password",
            {
                "type": Sequelize.STRING,
                "validate": {
                    "is": {}
                },
                "allowNull": false
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
