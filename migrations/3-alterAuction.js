'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * removeColumn "startCost" from table "Auctions"
 * addColumn "startBid" to table "Auctions"
 * changeColumn "password" on table "Users"
 *
 **/

var info = {
    "revision": 3,
    "name": "alterAuction",
    "created": "2018-05-19T17:13:45.826Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "removeColumn",
        params: ["Auctions", "startCost"]
    },
    {
        fn: "addColumn",
        params: [
            "Auctions",
            "startBid",
            {
                "type": Sequelize.INTEGER
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Users",
            "password",
            {
                "type": Sequelize.STRING,
                "validate": {
                    "is": {}
                },
                "allowNull": false
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
