'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * changeColumn "photo" on table "Lots"
 * changeColumn "photo" on table "Lots"
 * changeColumn "password" on table "Users"
 *
 **/

var info = {
    "revision": 8,
    "name": "lotDebug2",
    "created": "2018-05-24T14:04:27.787Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "changeColumn",
        params: [
            "Lots",
            "photo",
            {
                "type": Sequelize.STRING,
                "validate": {}
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Lots",
            "photo",
            {
                "type": Sequelize.STRING,
                "validate": {}
            }
        ]
    },
    {
        fn: "changeColumn",
        params: [
            "Users",
            "password",
            {
                "type": Sequelize.STRING,
                "validate": {
                    "is": {}
                },
                "allowNull": false
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
