import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { persistor, store } from './store';
import App from './modules/App';


import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
import 'react-datepicker/dist/react-datepicker.css';
import './style.css';

// CSS Modules, react-datepicker-cssmodules.css


render(
  <Provider store={store}>
    <PersistGate persistor={persistor}>
         <App/>
     </PersistGate>
  </Provider>,
  document.getElementById('root')
)
