import React from 'react'
import { Alert, Button } from 'react-bootstrap';

const ErrorAlert = (props) => ( <Alert bsStyle="danger">
   <h4>Oh snap! You got an error!</h4>
   <p>
     {props.message}
   </p>
 </Alert>
)


export default ErrorAlert
