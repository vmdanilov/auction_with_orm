import React from 'react'
import { withRouter } from 'react-router-dom'
import {  Navbar, Nav, NavItem, NavDropdown, MenuItem, Image } from 'react-bootstrap';
import { NavLink } from 'react-router-dom'
import ModalWin  from './modal'

class Header extends React.Component {
  constructor() {
    super()
    this.state = {
      activeLink: 0
    }
    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect(selectedKey) {
    this.setState( { activeLink: selectedKey } )
  }

  render() {
    let { logined, user } = this.props;

    return (
        <Navbar>
          <Navbar.Header>
            <NavLink to="/"><Image src={require("../../img/Logo2.jpg")} responsive /></NavLink>
          </Navbar.Header>

          <Nav pullRight activeKey={this.state.activeLink} onSelect={this.handleSelect}>
            <NavItem eventKey={1} href="#">Info</NavItem>
            <NavItem eventKey={2} href="#">Contact us</NavItem>
          {logined ?
             <NavDropdown eventKey={3} title={user.name} id="basic-nav-dropdown">
              <MenuItem eventKey={3.1} onClick={(e) => this.props.history.push(`/cabinet`)}>
                Cabinet
              </MenuItem>
              <MenuItem divider />
              <MenuItem eventKey={3.2} onClick={this.props.logOut}>Log out</MenuItem>
            </NavDropdown> :
            <NavItem href="#">
            <ModalWin></ModalWin>
            </NavItem>
            }
          </Nav>
        </Navbar>
      )
      }
}

export default withRouter(Header)
