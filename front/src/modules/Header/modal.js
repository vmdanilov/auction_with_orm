import React from 'react'
import { Button, Modal, Nav, NavItem } from 'react-bootstrap';
import { connect } from 'react-redux'
import { toggleAuthModal } from '../../actions/main'
import SignIn  from '../signIn'
import SignUp  from '../signUp'

export default class ModalWin extends React.Component {

  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.toogleSign = this.toogleSign.bind(this);

    this.state = {
      show: false,
      signIn: true
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  toogleSign(key) {
    this.setState({ signIn:key })
  }

  render() {
    return (
      <div>
        <Button bsStyle="info" onClick={this.handleShow}>
          Sign in
        </Button>

        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Nav bsStyle="pills" activeKey={this.state.signIn} onSelect={ this.toogleSign }>
              <NavItem eventKey={true} href="/home">
                Sign In
              </NavItem>
              <NavItem eventKey={false} title="Item">
                Sign Up
              </NavItem>
            </Nav>
          </Modal.Header>
          <Modal.Body>
            {this.state.signIn ? <SignIn></SignIn> : <SignUp onSuccess={this.toogleSign}></SignUp>}
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
