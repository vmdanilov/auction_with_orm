import { connect } from 'react-redux'
import { logOut } from '../../actions/user'
import Header from './Header'

const mapStateToProps = (state, ownProps) => ({
  logined: state.user.token,
  user:    state.user.info
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  logOut: () => {
    dispatch(logOut());
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header)
