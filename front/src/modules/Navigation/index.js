import React from 'react'
import { withRouter } from 'react-router-dom'
import api from '../api'

const NavigationPage =  ({history}) =>
	( <div className='navigation_container' >
		<div className='navigation'>
		<form className="form-inline text-center">
 			<div className="form-group">
	 			<h4>Find Auction</h4>
	 			<input type="text" className="form-control" onKeyPress={
						(e) => {
							if (e.key === 'Enter') {
								e.preventDefault();
									api.get(`/main/auctions/find?name=${e.target.value}`)
										.then(res => {
											console.log(res)
											history.push(`/auction/${res.data.id}`)
										})
											.catch(err => console.log(err))
							}
						}
					} placeholder="For example: Cisco..."/>
 			</div>
		</form>
	</div>
	</div>
	)

export default withRouter(NavigationPage);
