import React from 'react';
import { Panel,	ListGroup, ListGroupItem, Button,	FormGroup,InputGroup,	FormControl,Alert} from 'react-bootstrap';
import ErrorAlert from '../ErrorAlert'
import Timer from '../timer'
import SimpleReactValidator from "simple-react-validator";
import api from '../api';


class AuctionInfo extends React.Component {
	constructor(props) {
		super()
		this.initialState = {
			bid:0,
			error: '',
			winner:'',
		};
		this.state = this.initialState;
		this.validator = new SimpleReactValidator({
			last: {
				message: 'You were last who made a bid',
				rule: (val, options) => +options[0] != props.currentUser
			}
		});
		this.setBid = this.setBid.bind(this);
		this.submitBid = this.submitBid.bind(this);
	}

	setBid(event) {	this.setState({bid: event.target.value})}

	submitBid() {
		if (this.validator.allValid()) {
			this.props.emitBid(this.state.bid);
		} else {
			this.validator.showMessages();
			this.forceUpdate();
		}
	}

	componentWillReceiveProps() {
		this.setState({
			subscribed: this.props.auction.Users.indexOf(this.props.currentUser) !== -1,
		})
	}

	render() {
		let {auction, currentUser, lastBidder, currentPrice, participants, error, winner} = this.props
		const getAuctionState = (startDate, endDate) => {
			const now = new Date().getTime();
			let state = endDate.getTime() >= now;
			switch (state) {
				case true:
					if (startDate.getTime() <= now) return <span><Timer date={endDate}></Timer> <i>before end</i></span>
						else return <span><Timer date={startDate}></Timer> <i>before start</i></span>
					break;
				case false:
						return <span className='lot_price'><small>auction has been ended</small> Winner: {winner}</span>
					break;
				default:
					return 'something goes wrong'
		}
		}

		new Date(auction.endDate).getTime() > new Date().getTime() ?
		<span><Timer date={new Date(auction.endDate)}></Timer> <i>before end</i></span> :
		<span className='lot_price'><small>auction has been ended</small> Winner: {this.state.winner}</span>
		return (<Panel bsStyle="info">
			<Panel.Heading>
				<Panel.Title componentClass="h3">Auction info</Panel.Title>
			</Panel.Heading>
			<Panel.Body>
				{
					this.state.subscribed ?
						<Button bsStyle="default" block="block"
						 	onClick={() => {
								this.props.cancelReg(auction.id, currentUser);
								this.setState({subscribed:false})
							}}>
								Unsubscribe
							</Button> :
							<Button bsStyle="info" block="block"
								onClick={() => {
									this.props.registerToAuction(auction.id, currentUser)
									this.setState({subscribed:true})
								}}
								disabled={auction.Lot.UserId === currentUser}>
								Subscribe
							</Button>
				}
				<ListGroup>
					<ListGroupItem header="Start price">{auction.startBid}$</ListGroupItem>
					<ListGroupItem header="Min Step">{auction.minStep}$</ListGroupItem>
					<ListGroupItem header="Start date">
						{auction.startDate.slice(0, 10)}<br/>
						At: {auction.startDate.slice(11, 16)}
					</ListGroupItem>
					<ListGroupItem header="End date">
						{auction.endDate.slice(0, 10)}
						<br/>At: {auction.endDate.slice(11, 16)}
					</ListGroupItem>
					<ListGroupItem header="Participants">{participants.length}</ListGroupItem>
					<ListGroupItem header="Current Price">
						<span className="bg-info text-center lot_price" >
							{currentPrice}$
						</span>
					</ListGroupItem>
					<ListGroupItem className="bg-info text-center lot_timer">
						{new Date(auction.startDate).getFullYear() == 1990 ? null :
							getAuctionState(new Date(auction.startDate), new Date(auction.endDate))	}
					</ListGroupItem>
				</ListGroup>
				<FormGroup>

					{
						currentUser ?
							<InputGroup>
									<InputGroup.Addon>$</InputGroup.Addon>

									<FormControl type="text" value={this.state.bid} onChange={this.setBid}/>
									<InputGroup.Addon>.00</InputGroup.Addon>
									<InputGroup.Button>
										<Button bsStyle="default" disabled={auction.Lot.UserId === currentUser}
											 onClick={this.submitBid }>
											Bid
										</Button>
									</InputGroup.Button>
								</InputGroup> :
							 <Alert bsStyle="warning">
									<strong>Attention!</strong>
									Sign in to take part in auction.
								</Alert>
					}
					{
						error ? <ErrorAlert message={error}></ErrorAlert> : null
					}
					{this.validator.message('bid', this.state.bid, `integer|required|last:${lastBidder}|gte:${auction.minStep}`, 'text-danger')}
				</FormGroup>
			</Panel.Body>
		</Panel>)
	}
}

export default AuctionInfo;
