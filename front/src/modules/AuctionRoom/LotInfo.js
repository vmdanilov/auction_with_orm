import React from 'react';
import { Thumbnail } from 'react-bootstrap';

const LotInfo = ({Lot}) => {
	return (
		<Thumbnail src={ Lot.photo }  alt="242x200">
			<h3>{ Lot.name }</h3>
			<p>{ Lot.info }</p>
			<a href={ Lot.doc } download>
				Documentation
				<img className="erase_margin" src={require("../../img/doc.png")} alt="documentation"></img>
			</a>
		</Thumbnail>
	)
}

export default LotInfo;
