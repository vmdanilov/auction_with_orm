import React from 'react'
import { connect } from 'react-redux'
import socketIOClient from "socket.io-client";
import api from '../api';
import { userRegistered, userCancelReg, } from '../../actions/main';

import { Grid, Row, Col, PageHeader, Alert } from 'react-bootstrap';
import Header from '../Header'
import LotInfo from './LotInfo'
import AuctionInfo from './AuctionInfo'


class AuctionRoom extends React.Component {

	constructor(props) {
		super();
		this.initialState = {
			auction: {
        id: 1,
        startDate: "1990-06-24T13:50:51.000Z",
        endDate: "2290-06-25T13:50:51.000Z",
        startBid: 0,
        minStep: 0,
        LotId: 1,
        Users: [],
				Lot: {
						id: 1,
						name: "lot",
						photo: "http://localhost:8000/public/photo/1.jpg",
						doc: "",
						info: "",
						UserId: 0,
						createdAt: "2018-05-24T13:50:50.000Z",
						updatedAt: "2018-05-24T13:50:50.000Z"
				},
			},
			currentPrice: 0,
			participants: [],
			lastBidder: null,
			error: ''
	}

	this.state = this.initialState;
	this.emitBid = this.emitBid.bind(this);
	this.showWinner = this.showWinner.bind(this);

	this.endpoint = "http://127.0.0.1:8000";
	this.socket = socketIOClient(this.endpoint, {
				query: {
					token: props.token
				}
	});
}

showWinner(data) {
	if (data.lastBet) {
		api.get(`/user/profile/${data.lastBet.userId}`)
			.then(res => this.setState( {
				 winner: res.data.user.email,
				 currentPrice: this.state.currentPrice + data.amount,
				 participants: data.players,
			 }))
			.catch(err => console.log(err.response.data))
	} else {
		this.setState( {
			 winner: 'no one',
			 currentPrice: this.state.currentPrice + data.amount,
			 participants: data.players,
		 })
	}

}

emitBid(amount, userId=this.props.currentUser) {
	this.socket.emit('makeBit', {
		amount,
		userId: this.props.currentUser,
		auctionId: this.state.auction.id
	})
}

componentDidMount() {
		api.get(`/main/auctions/show/${this.props.match.params.id}`)
			.then(({data} )=> {
				this.setState({
					auction: data.auction,
					currentPrice: data.auction.startBid
				})

				this.socket.emit('room', this.state.auction.id);

				this.socket.on('joinRoom', data => {
					if (new Date().getTime() >= new Date(this.state.auction.endDate).getTime()) {
						this.showWinner(data)
					} else {
						this.setState({
							currentPrice: this.state.currentPrice + data.amount,
							participants: data.players,
							lastBidder: data.lastBet ? data.lastBet.userId : null
						})
					}

				})

				this.socket.on("unauthorized", (error) => {
					this.setState({ error	})
				});

				this.socket.on('Error', data => {
					this.setState({
						error: data.error.message
					})
				})

		    this.socket.on("changePrice", data => {
					this.setState({ currentPrice: this.state.currentPrice + +data.bet.amount, lastBidder: data.bet.userId});
					this.state.participants.indexOf(data.bet.userId)  === -1 ?
					this.setState({participants: [...this.state.participants, data.bet.userId]}) : null
				});

			})
	}

	render() {
		return (
			<div>
			<Header></Header>
			<PageHeader className='text-center text-muted'>
				Bid event
			</PageHeader>
			<Grid>
				<Row>
					<Col xs={6} md={8}>
						<LotInfo Lot={this.state.auction.Lot} />
					</Col>
					<Col xs={6} md={4}>
						<AuctionInfo
							auction={this.state.auction}
							currentUser={this.props.currentUser}
							currentPrice={this.state.currentPrice}
							participants={this.state.participants}
							lastBidder={this.state.lastBidder}
							emitBid={this.emitBid}
							registerToAuction={this.props.registerToAuction}
							cancelReg={this.props.cancelReg}
							error={this.state.error}
							winner={this.state.winner}
							 />
					</Col>
				</Row>
			</Grid>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
  currentUser: state.user.info.id,
	token: state.user.token,

})

const mapDispatchToProps = (dispatch, ownProps) => ({
	registerToAuction: (auctionId, currentUser) => {
		api.post(`/main/auctions/register/${auctionId}`).then(res => {
			dispatch(userRegistered(auctionId, currentUser));
		})
	},
	cancelReg: (auctionId, currentUser) => {
		api.post(`/main/auctions/register/cancel/${auctionId}`).then(res => {
			dispatch(userCancelReg(auctionId, currentUser));
		})
	},
})

export default connect(mapStateToProps, mapDispatchToProps)(AuctionRoom)
