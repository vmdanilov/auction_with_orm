import React from 'react';
import SimpleReactValidator from "simple-react-validator";
import {  Form, FormGroup, Col, Button, ControlLabel } from 'react-bootstrap';
import axios from 'axios';

export default class signUp extends React.Component {
  constructor() {
     super();
	   this.validator = new SimpleReactValidator({
      confirm:
        {
        message: 'The confirm should be equal to password',
        rule: (val, options) => this.state.password === this.state.confirm
        }
    });
     this.submitForm = this.submitForm.bind(this);
     this.setEmail = this.setEmail.bind(this);
     this.setPassword = this.setPassword.bind(this);
     this.setConfirm = this.setConfirm.bind(this);
     this.setName = this.setName.bind(this);
     this.setPhone = this.setPhone.bind(this);

     this.initialState = {
       email: '',
       password: '',
       confirm: '',
       name: '',
       phone: ''
      }
     this.state = this.initialState;
   }

   setEmail(event) { this.setState({ email: event.target.value }) }
   setPassword(event) { this.setState({ password: event.target.value }) }
   setName(event) { this.setState({ name: event.target.value }) }
   setPhone(event) { this.setState({ phone: event.target.value }) }
   setConfirm(event) { this.setState({ confirm: event.target.value }) }



submitForm() {
	if (this.validator.allValid()) {
    axios.post('http://localhost:8000/signup', this.state)
      .then(res => {
        console.log(res);
        this.props.onSuccess(true);
      })
      .catch(err => alert(err.response.data.message.join(',')))
	} else {
		this.validator.showMessages();
		// rerender to show messages for the first time
		this.forceUpdate();
	}
}

  render() {
    return (
      <Form>
      <FormGroup>
        <Col componentClass={ControlLabel} sm={2}>Name</Col>
        <input className="form-control" value={this.state.name} onChange={this.setName} />
        {this.validator.message('name', this.state.name, 'required|alpha', 'text-danger')}
      </FormGroup>
      <FormGroup>
        <Col componentClass={ControlLabel} sm={2}>Phone</Col>
        <input className="form-control" value={this.state.phone} onChange={this.setPhone} />
        {this.validator.message('phone', this.state.phone, 'required|phone', 'text-danger')}
      </FormGroup>
      <FormGroup>
        <Col componentClass={ControlLabel} sm={2}>Email</Col>
        <input className="form-control" value={this.state.email} onChange={this.setEmail} />
        {this.validator.message('email', this.state.email, 'required|email', 'text-danger')}
      </FormGroup>
      <FormGroup>
        <Col componentClass={ControlLabel} sm={2}>Password</Col>
        <input  className="form-control" type="password" value={this.state.password} onChange={this.setPassword} />
        {this.validator.message('password', this.state.password, 'required|min:8|alpha_num', 'text-danger')}
      </FormGroup>
      <FormGroup>
        <Col componentClass={ControlLabel} sm={2}>Confirm password</Col>
        <input  className="form-control" type="password" value={this.state.confirm} onChange={this.setConfirm} />
        {this.validator.message('confirm', this.state.confirm, 'required|min:8|confirm', 'text-danger')}
      </FormGroup>
      <FormGroup>
		         <Button bsStyle="primary" onClick={this.submitForm}  block>Sign up</Button>
      </FormGroup>
    </Form>
      )

}
}
