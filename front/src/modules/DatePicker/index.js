import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';


export default class CustomDatePicker extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      startDate: moment()
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(date) {
    this.setState({
      startDate: date
    });
    console.log(date);
    date ? this.props.returnDate(date._d) : this.props.returnDate('');
  }

  render() {
    return <DatePicker
        placeholderText="Select date"
        className={this.props.inputClass}
        showTimeSelect
        timeFormat="HH:mm"
        timeIntervals={15}
        dateFormat="LLL"
        timeCaption="time"
        isClearable={true}
        selected={this.state.startDate}
        onChange={this.handleChange}
        calendarClassName="date_pick"
    />;
  }
}
