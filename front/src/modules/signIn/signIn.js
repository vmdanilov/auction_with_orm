import React from 'react';
import SimpleReactValidator from "simple-react-validator";
import {  Form, FormGroup, Col, Button, ControlLabel } from 'react-bootstrap';


export default class signIn extends React.Component {
  constructor() {
     super();
	   this.validator = new SimpleReactValidator();
     this.submitForm = this.submitForm.bind(this);
     this.setEmail = this.setEmail.bind(this);
     this.setPassword = this.setPassword.bind(this);

     this.state = {
       email: '',
       password: ''
     }
   }

   setEmail(event) { this.setState({ email: event.target.value }) }
   setPassword(event) { this.setState({ password: event.target.value }) }



submitForm() {
	if (this.validator.allValid()) {
    this.props.login(this.state.email, this.state.password)
		this.validator.showMessages();
		// rerender to show messages for the first time
		this.forceUpdate();
	}
}

  render() {
    return (
			<Form horizontal>
				<FormGroup controlId="formHorizontalEmail">
					<Col componentClass={ControlLabel} sm={2}>
						Email
					</Col>
					<Col sm={10}>
						<input className="form-control" value={this.state.email} onChange={this.setEmail} ref="email" />
            {this.validator.message('email', this.state.email, 'required|email', 'text-danger')}
					</Col>
			</FormGroup>
			<FormGroup controlId="formHorizontalPassword">
				<Col componentClass={ControlLabel} sm={2}>
					Password
				</Col>
				<Col sm={10}>
					<input  className="form-control" type="password" placeholder="Password" onChange={this.setPassword} ref="password" />
          {this.validator.message('password', this.state.password, 'required|min:8|alpha_num', 'text-danger')}
				</Col>
			</FormGroup>
      <FormGroup>
	       <Col sm={12}>
		          <Button bsStyle="primary" onClick={this.submitForm}  block>Sign in</Button>
	       </Col>
      </FormGroup>
    </Form>
      )

}
}
