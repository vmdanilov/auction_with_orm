import { connect } from 'react-redux'
import { fetchLogin } from '../../actions/user'
import signIn from './signIn'
import api from '../api';

const mapStateToProps = (state, ownProps) => ({
  user: state
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  login: (email, password) => {
    api.post('http://localhost:8000/login', { email, password })
      .then(res => {
        dispatch(fetchLogin(res.data.user, res.data.token));
      })
      .catch(err => alert(err.response.data))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(signIn)
