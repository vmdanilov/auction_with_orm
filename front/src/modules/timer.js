import React from 'react';
import moment from 'moment';

export default class Timer extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
			days: 0,
			hours: 0,
      minutes: 0,
			seconds: 0
    };
		this.interval = 0;
		this.tick = this.tick.bind(this);
	}

  tick(countDownDate) {
  	countDownDate = countDownDate.getTime();
  	this.interval = setInterval(() => {
  		let now = new Date().getTime();
  		let distance = countDownDate - now;

  		let days = Math.floor(distance / (1000 * 60 * 60 * 24));
  		let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  		let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  		let seconds = Math.floor((distance % (1000 * 60)) / 1000);
  		// Display the result in the element with id="demo"
			this.setState({ days, hours, minutes, seconds})

  		if (distance < 0) clearInterval(this.interval);
  	}, 1000)
  }

	componentDidMount() {
		this.tick(this.props.date)
	}

	componentWillUnmount() {
		clearInterval(this.interval)
	}

  render() {
    return <span> {this.state.days}d {this.state.hours}h {this.state.minutes}m {this.state.seconds}s</span>

  }
}
