import React from 'react'
import { Pagination } from 'react-bootstrap';


export default class CustomPagination extends React.Component {

  constructor(props) {
    super(props)

    this.handleTooglePage = this.handleTooglePage.bind(this);
    this.state = {
      active: 1
    }
  }

  handleTooglePage(number) {
    this.setState({active: number});
    this.props.getPage(number);
  }

  render(){
    let {pageNumber} = this.props;
    let pages = [];
    for (let i = 1; i <= pageNumber; i++) {
      pages.push(
        <Pagination.Item active={ i === this.state.active } key={i}
        onClick={() => this.handleTooglePage(i)}>
        {i}
      </Pagination.Item>
    )
    }

    return ( <Pagination>
    {
      this.state.active === 1 ? <Pagination.First disabled></Pagination.First> :
      <Pagination.First onClick={() => this.handleTooglePage(1)} />
    }
    {
      this.state.active === 1 ? <Pagination.Prev disabled></Pagination.Prev> :
      <Pagination.Prev  onClick={() => this.handleTooglePage(this.state.active - 1)} />
    }
      { pages }
    {
      this.state.active === pageNumber ?
      <Pagination.Next disabled></Pagination.Next> :
      <Pagination.Next  onClick={() => this.handleTooglePage(this.state.active + 1)} />
    }
    {
     this.state.active === pageNumber ?
     <Pagination.Last disabled></Pagination.Last> :
     <Pagination.Last  onClick={() => this.handleTooglePage(pageNumber)} />
    }
  </Pagination>
    );
  }

}
