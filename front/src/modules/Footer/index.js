import React from 'react'

class Footer extends React.Component {
  constructor() {
    super()
    this.state = {
      activeLink: 0
    }
    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect(selectedKey) {
    this.setState( { activeLink: selectedKey } )
  }

  render() {
    return (
      <footer>
        <nav>
      <a>HOME</a>
      <a>CONTACT</a>
      <a>ABOUT</a>
      <a>SERVICES</a>
        </nav>

<div className="logo">
    <a><img className="graficlogo" alt=""/></a>
</div>
<div className="social">
    <a><img alt='must have' src={require("../../img/em.png")}/></a>
    <a><img alt='must have' src={require("../../img/face.png")}/></a>
    <a><img alt='must have' src={require("../../img/goo.png")}/></a>
    <a><img alt='must have' src={require("../../img/inst.png")}/></a>
    <a><img alt='must have' src={require("../../img/pint.png")}/></a>
</div>
<p>Vironit lab. 2018</p>

</footer>
      )
      }
}

export default Footer
