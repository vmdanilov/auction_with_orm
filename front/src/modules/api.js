import axios from 'axios';
import {store} from '../store'


let api = () => {
	let token = 'JWT '+ store.getState().user.token;
	let instance = axios.create({
		baseURL: 'http://localhost:8000',
		timeout: 15000,
	})
	instance.defaults.headers.common['Authorization'] = token;
	return instance;
}

store.subscribe(api);


export default api();
