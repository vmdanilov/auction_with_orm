import { connect } from 'react-redux'
import { gotAllAuctions, userRegistered, userCancelReg } from '../../actions/main';
import AuctionsList from './AuctionsList'
import api from '../api';

const mapStateToProps = (state, ownProps) => ({
  auctions: state.main.allAuctions,
	count: state.main.count,
  filter: state.main.filter,
	currentUser: state.user.info.id,
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  getAuctions: (page = 1, filter) => {
    let query;
    Object.keys(filter).length === 0 ?
    query = `page=${page}` :
    query = `page=${page}&startDate=${filter.startDate}&endDate=${filter.endDate}&categories=${filter.categories ? filter.categories.join(',') : ''}`

    api.get('/main/auctions/all?'+query)
      .then(res => {
				dispatch( gotAllAuctions(res.data.auctions, res.data.count) );
      })
      .catch(err => console.log(err))
  },
	registerToAuction: (auctionId, currentUser) => {
		api.post(`/main/auctions/register/${auctionId}`)
			.then(res => {
				dispatch( userRegistered(auctionId, currentUser) );
			})
	},
	cancelReg: (auctionId, currentUser) => {
		api.post(`/main/auctions/register/cancel/${auctionId}`)
			.then(res => {
				dispatch( userCancelReg(auctionId, currentUser) );
			})
	},
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuctionsList)
