import React from 'react'
import { Row, Col } from 'react-bootstrap';
import AuctUnit from './AuctUnit'
import Filter from '../Filter';
import CustomPagination from '../CustomPagination';

class AuctionsList extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
 			value: '',
			filter: {}
		}
		this.turnPage = this.turnPage.bind(this);
		this.implementFilter = this.implementFilter.bind(this);
	}

	componentWillMount() {
		this.props.getAuctions(1, this.state.filter);
	}

	turnPage(page) {
		this.props.getAuctions(page, this.state.filter);
	}

	implementFilter(filter) {
		this.setState({filter});
		this.props.getAuctions(undefined, filter);
	}

	render() {
		let auctions = this.props.auctions;
		let pageNumber = Math.ceil(this.props.count/8);
		return (<div>
				<Filter implementFilter={this.implementFilter} />
				  <Row>
						{ auctions.map( ( auct, i) => {
							return <AuctUnit
									auction={auct}
								 	currentUser={this.props.currentUser}
								 	register={this.props.registerToAuction}
									cancelReg={this.props.cancelReg}
									key={auct.id}
								  ></AuctUnit>
						}) }
				  </Row>
				<CustomPagination pageNumber={pageNumber} getPage={this.turnPage}></CustomPagination>
		</div>

		)
	}
}

export default AuctionsList
