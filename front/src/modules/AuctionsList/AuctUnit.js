import React from 'react'
import { Col, Thumbnail, Button, Label } from 'react-bootstrap';
import { withRouter } from 'react-router-dom'

const AuctUnit =  ({auction, currentUser, register, cancelReg, history}) => {
	let { Lot } = auction;
	console.log();
	return (	<Col xs={6} md={3} >
      <Thumbnail src={ Lot.photo }  alt="242x200" className='auction-unit'>
        <h3>{ Lot.name }</h3>
        <p>{ Lot.info.slice(0,250) }</p>
				<br/>
        <h4><Label>{ auction.startDate.slice(0, 10) + ' at '+ auction.startDate.slice(11, 16)}
				</Label> starts</h4>
				<h4><Label>{ auction.endDate.slice(0, 10) + ' at '+ auction.endDate.slice(11, 16)}
				</Label> ends</h4>
        <p>
					{auction.Users.indexOf(currentUser) === -1 ?
						<Button bsStyle="info" onClick={() => register(auction.id, currentUser)}
							disabled={Lot.UserId === currentUser}
							>
							Subscribe</Button> :
						<Button bsStyle="warning" onClick={() => cancelReg(auction.id, currentUser)}>Unsubscribe</Button>
					}

          <Button bsStyle="default" onClick={(e) => history.push(`/auction/${auction.id}`)}>
						Join auction
					</Button>
        </p>
      </Thumbnail>
		</Col>
	)
}
export default withRouter(AuctUnit);
