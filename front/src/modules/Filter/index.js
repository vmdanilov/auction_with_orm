import React from 'react'
import {Row, Col, FormGroup, ControlLabel, Button } from 'react-bootstrap';
import CustomDatePicker from '../DatePicker'
import Chips from 'react-chips'
import moment from 'moment';

let cat = [
	'communication',
	'power',
	'electic'
]

export default class Filter extends React.Component {
	constructor(props) {
		super();

		this.initialState = {
			startDate: moment()._d,
			endDate: moment()._d,
			categories: []
		}
		this.state = this.initialState;

		this.setStartDate = this.setStartDate.bind(this);
		this.setEndDate = this.setEndDate.bind(this);
		this.changeChips = this.changeChips.bind(this);
	}

	setStartDate(date) { this.setState({startDate: date}) }
	setEndDate(date) { this.setState({endDate: date}) }
	changeChips(categories){ this.setState({ categories }); }

	render() {
		let {implementFilter} = this.props;

		return ( <Row>

			<Col md={4} xs={12}>
				<FormGroup>
					<ControlLabel>Type categories</ControlLabel>
					<Chips
						placeholder="communication, routing.."
						value={this.state.categories}
						onChange={this.changeChips}
						suggestions={['communication', 'power', 'routing']}
					/>
				</FormGroup>
				</Col>
			<Col md={4} xs={12}>
			<FormGroup>
				<ControlLabel>Select start Date</ControlLabel>
				<CustomDatePicker inputClass={'user_input'} returnDate={ this.setStartDate }></CustomDatePicker>
			</FormGroup>
			</Col>
			<Col md={4} xs={12}>
			<FormGroup>
				<ControlLabel>Select end Date</ControlLabel>
				<CustomDatePicker inputClass={'user_input'} returnDate={ this.setEndDate }></CustomDatePicker>
			</FormGroup>
			</Col>
			<Col md={12} xs={12}>
			<FormGroup>
				<Button bsStyle="default" onClick={() => implementFilter(this.state)}>Filter</Button>
			 <Button bsStyle="default" onClick={() => implementFilter({})}>Cancel Filter</Button>
			 </FormGroup>
		 </Col>
	    </Row>
	  );
	}
}
