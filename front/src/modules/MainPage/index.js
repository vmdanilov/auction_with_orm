import React from 'react'
import {  Grid, Row, Col, PageHeader } from 'react-bootstrap';
import AuctionsList from '../AuctionsList';
import Filter from '../Filter';

export default class MainPage extends React.Component {
	constructor(props) {
		super();

		this.initialState = {
			filter: {}
		}
		this.state = this.initialState;

		this.implementFilter = this.implementFilter.bind(this);
	}

	implementFilter(conditions) {
		this.setState({filter: conditions})
	}

	render() {
		return ( <div>
			<PageHeader className='text-center'>
				Best offers <small>find your one</small>
			</PageHeader>
			<Grid>
				<Row>
						<AuctionsList />
				</Row>
			</Grid>

		</div>
		)
	}
}
