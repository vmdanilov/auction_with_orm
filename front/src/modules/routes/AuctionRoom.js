import React from 'react'
import {
	 	Grid, Row, Col, PageHeader, Thumbnail,
	 	Panel,ListGroup, ListGroupItem, Button,
	  FormGroup,InputGroup, FormControl, Alert
	} from 'react-bootstrap';
import Header from '../Header'
import { connect } from 'react-redux'
import { userRegistered, userCancelReg, } from '../../actions/main';
import api from '../api';
import socketIOClient from "socket.io-client";
import SimpleReactValidator from "simple-react-validator";
import ErrorAlert from '../ErrorAlert'

class AuctionRoom extends React.Component {
	constructor(props) {
		super()

		this.endpoint  = "http://127.0.0.1:8000";
		this.socket = socketIOClient(this.endpoint, {
			query: {
				token: props.token
			}
		});

		this.validator = new SimpleReactValidator({
			last:
				{
				message: 'You were last who made a bid',
				rule: (val, options) => this.state.lastBidder !== props.currentUser
			}
		});
		this.initialState = {
			error: '',
			auction: {},
			currentPrice: props.auctions.filter(auct => auct.id == props.match.params.id )[0].startBid,
			participants: 0,
			bid: props.auctions.filter(auct => auct.id == props.match.params.id )[0].minStep,
			lastBidder: null,
 		};
		this.state = this.initialState;

		this.auctionId = props.auctions.filter(auct => auct.id == props.match.params.id )[0].id
		this.setBid = this.setBid.bind(this);
		this.submitBid = this.submitBid.bind(this);

	}

	setBid(event) { this.setState({ bid: event.target.value }) }

	submitBid() {
		if (this.validator.allValid()) {
			 this.socket.emit('makeBit', {
				 amount: +this.state.bid,
				 userId: this.props.currentUser,
				 auctionId: this.auctionId,
			 })
		} else {
			this.validator.showMessages();
			this.forceUpdate();
		}
	}

	componentDidMount() {
		let auctionId = this.props.auctions.filter(auct => auct.id == this.props.match.params.id )[0].id
		api.get(`/main/auctions/show/${this.props.match.params.id}`)
			.then(auction => {
				console.log(auction);
			})

		this.socket.emit('room', auctionId);

		this.socket.on('joinRoom', data => {
			this.setState({
				currentPrice: this.state.currentPrice + data.amount,
				participants: data.players,
				lastBidder: data.lastBet ? data.lastBet.userId : null
			})
		})

		this.socket.on("unauthorized", (err) => {
			this.setState({
				errMessage: err,
				error: true
			})
		});

		this.socket.on('Error', data => {
			this.setState({
				errMessage: data.error.message,
				error: true
			})
			console.log(this.state);
		})

    this.socket.on("changePrice", data => {
			this.setState({ currentPrice: this.state.currentPrice + data.bet.amount, lastBidder: data.bet.userId});
			this.state.participants.indexOf(data.bet.userId)  === -1 ?
			this.setState({participants: [...this.state.participants, data.bet.userId]}) : null
		});

	}

	render(){
		let {currentUser, registerToAuction, cancelReg, auctions, match} = this.props;
		let { currentPrice, participants } = this.state;
		let auction = auctions.filter(auct => auct.id == match.params.id )[0];

		return ( <div>
			<Header></Header>
			<PageHeader className='text-center text-muted'>
				Bid event
			</PageHeader>
			<Grid>
				<Row>
					<Col xs={6} md={8}>
						<Thumbnail src={ auction.Lot.photo }  alt="242x200">
		        	<h3>{ auction.Lot.name }</h3>
		        	<p>{ auction.Lot.info }</p>
							<a href={ auction.Lot.doc } download>
								Documentation
								<img className="erase_margin" src={require("../../img/doc.png")} alt="documentation"></img>
							</a>
		      	</Thumbnail>
					</Col>
					<Col xs={6} md={4}>
						<Panel bsStyle="info">
    					<Panel.Heading>
      					<Panel.Title componentClass="h3">Auction info</Panel.Title>
    					</Panel.Heading>
    					<Panel.Body>
								{
									auction.Users.indexOf(currentUser) === -1 ?
									<Button bsStyle="info" block
										onClick={() => registerToAuction(auction.id, currentUser)}
										disabled={auction.Lot.UserId === currentUser}>
										Subscribe
									</Button> :
									<Button bsStyle="default" block
													onClick={() => cancelReg(auction.id, currentUser)}>
													Unsubscribe
									</Button>
											}
							<ListGroup>
  							<ListGroupItem header="Start price">{ auction.startBid }$</ListGroupItem>
  							<ListGroupItem header="Min Step">{ auction.minStep }$</ListGroupItem>
  							<ListGroupItem header="Start date">
									{ auction.startDate.slice(0, 10)}<br/> At: {auction.startDate.slice(11, 16)}
								</ListGroupItem>
  							<ListGroupItem header="End date">
									{ auction.endDate.slice(0, 10)} <br/>At: {auction.endDate.slice(11, 16) }
								</ListGroupItem>
  							<ListGroupItem header="Participants">{ participants.length }</ListGroupItem>
  							<ListGroupItem header="Current Price" >
									<h4 className="lot_price bg-info text-center"
										 style={{padding:'10px', borderRadius:'2px', color:"#575757"}}>
										 { currentPrice }$
									 </h4>
									</ListGroupItem>
							</ListGroup>

								<FormGroup>

									{currentUser ?
										<InputGroup>
											<InputGroup.Addon>$</InputGroup.Addon>

											<FormControl type="text" value={this.state.bid} onChange={this.setBid} />
											<InputGroup.Addon>.00</InputGroup.Addon>
											<InputGroup.Button>
													<Button bsStyle="default"
													disabled={auction.Lot.UserId === currentUser}
													onClick={this.submitBid}
													>
													Bid
													</Button>
			      					</InputGroup.Button>
										</InputGroup> :
										<Alert bsStyle="warning">
  									<strong>Attention!</strong> Sign in to take part in auction.
									</Alert> }
									{this.state.error ? <ErrorAlert message={this.state.errMessage}></ErrorAlert> : null}
									{this.validator.message('bid', this.state.bid, `integer|required|last|gte:${auction.minStep}`, 'text-danger')}
								</FormGroup>
    				</Panel.Body>
  				</Panel>
				</Col>

			</Row>
		</Grid>

	</div>
	)}
}

const mapStateToProps = (state, ownProps) => ({
  currentUser: state.user.info.id,
	auctions: [...state.main.allAuctions, ...state.user.auctions, ...state.user.regAuctions],
	token: state.user.token,

})

const mapDispatchToProps = (dispatch, ownProps) => ({
	registerToAuction: (auctionId, currentUser) => {
		api.post(`/main/auctions/register/${auctionId}`).then(res => {
			dispatch(userRegistered(auctionId, currentUser));
		})
	},
	cancelReg: (auctionId, currentUser) => {
		api.post(`/main/auctions/register/cancel/${auctionId}`).then(res => {
			dispatch(userCancelReg(auctionId, currentUser));
		})
	},
})

export default connect(mapStateToProps, mapDispatchToProps)(AuctionRoom)
