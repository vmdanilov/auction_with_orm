import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './Home'
import Cabinet from './Cabinet'
// import AuctionRoom from './AuctionRoom'
import AuctionRoom from '../AuctionRoom';

export default () =>
(<BrowserRouter>
		<Switch>
			<Route path='/' exact component={ Home }/>
			<Route path='/cabinet' exact component={ Cabinet }/>
			<Route path='/auction/:id' exact component={ AuctionRoom }/>
		</Switch>
	</BrowserRouter>
)
