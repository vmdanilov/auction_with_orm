import React from 'react'
import { PageHeader, Tabs, Tab } from 'react-bootstrap';
import Header from '../Header'
import Footer from '../Footer'
import Profile from '../UserProfile';
import Assets from '../UserAssets';
import Auctions from '../UserAuctions';

const Cabinet =  (props) =>
	( <div><Header></Header>
		<div className="container">
		<PageHeader className='text-muted'>Dashboard</PageHeader>
			<Tabs defaultActiveKey={1} animation={false} id="noanim-tab-example">
	  		<Tab eventKey={1} title="Profile">
	    		<Profile></Profile>
	  		</Tab>
	  		<Tab eventKey={2} title="Assets">
	    		<Assets></Assets>
	  		</Tab>
	  		<Tab eventKey={3} title="Auctions">
	    		<Auctions></Auctions>
	  		</Tab>
			</Tabs>
	</div>
	<Footer></Footer>
	</div>
	)

export default Cabinet;
