import React from 'react';

import Header from '../Header'
import Footer from '../Footer'
import Navigation from '../Navigation';
import MainPage from '../MainPage';

export default () => (
	<div>
		<Header></Header>
		<Navigation></Navigation>
		<MainPage></MainPage>
		<Footer></Footer>
	</div>
	)
