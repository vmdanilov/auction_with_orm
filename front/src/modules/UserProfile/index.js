import { connect } from 'react-redux'
import Profile from './Profile'
import { editProfile } from '../../actions/user'
import api from '../api';


const mapStateToProps = (state, ownProps) => ({
  user: state.user.info
})

const mapDispatchToProps = (dispatch, ownProps) => ({
	infoEdit : (info) => {
		api.post('/user/profile/edit', info)
			.then(res => {
				dispatch(editProfile(res.data.user));
				alert('success')
			})
			.catch(err => console.log(err))
	},

	emailEdit: (email) => {
		api.post('/user/profile/edit', {email})
			.then(res => {
				alert('success')
			})
			.catch(err => console.log(err))
	 },

	passwordEdit: (data) => {
		api.post('/user/profile/edit/password', data)
			.then(res => {
				alert(res.data.message)
				console.log(res);
			})
			.catch(err => alert(err.response.data))
		 }
	 })

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile)
