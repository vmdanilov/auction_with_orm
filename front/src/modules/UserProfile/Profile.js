import React from 'react'
import { Col, Jumbotron, FormGroup, ControlLabel, Button, Form } from 'react-bootstrap';
import SimpleReactValidator from "simple-react-validator";

export default class Profile extends React.Component {

  constructor() {
     super();
	   this.validator = new SimpleReactValidator({
      password_confirm:
        {
        message: 'The password confirm should be equal to password',
        rule: (val, options) => this.state.newPassword === this.state.passwordConfirm
			},
      email_confirm:
        {
        message: 'The email confirm should be equal to new email',
        rule: (val, options) => this.state.newEmail === this.state.emailConfirm
			},
    });
		this.submitInfo = this.submitInfo.bind(this);
		this.submitEmail = this.submitEmail.bind(this);
		this.submitPassword = this.submitPassword.bind(this);

		this.setName = this.setName.bind(this);
		this.setSurname = this.setSurname.bind(this);
		this.setPhone = this.setPhone.bind(this);
		this.setCompany = this.setCompany.bind(this);
		this.setAddress = this.setAddress.bind(this);

		this.setNewEmail = this.setNewEmail.bind(this);
    this.setEmailConfirm = this.setEmailConfirm.bind(this);

    this.setPassword = this.setPassword.bind(this);
    this.setNewPassword = this.setNewPassword.bind(this);
    this.setPasswordConfirm = this.setPasswordConfirm.bind(this);
		this.state = {
       name: '',
       surname: '',
			 company: '',
       phone: '',
			 address: '',
			 newEmail: '',
			 emailConfirm: '',
			 password: '',
			 newPassword: '',
			 passwordConfirm: '',

     }
   }

	 componentWillMount() {
		 this.setState({
			 name: this.props.user.name,
			 surname: this.props.user.surname,
			 company: this.props.user.company,
			 phone: this.props.user.phone,
			 address: this.props.user.address,
		 })
	 }
	 setName(event) { this.setState({ name: event.target.value }) }
	 setSurname(event) { this.setState({ surname: event.target.value }) }
	 setCompany(event) { this.setState({ company: event.target.value }) }
	 setPhone(event) { this.setState({ phone: event.target.value }) }
	 setAddress(event) { this.setState({ address: event.target.value }) }

   setNewEmail(event) { this.setState({ newEmail: event.target.value }) }
   setEmailConfirm(event) { this.setState({ emailConfirm: event.target.value }) }

   setPassword(event) { this.setState({ password: event.target.value }) }
   setNewPassword(event) { this.setState({ newPassword: event.target.value }) }
   setPasswordConfirm(event) { this.setState({ passwordConfirm: event.target.value }) }

	 submitInfo() {
		let {name, surname, company, phone, address} = this.validator.fields;
 		if (name && surname && company && phone && address) {
			this.props.infoEdit({
				name: this.state.name,
				surname: this.state.surname,
				company: this.state.company,
				address: this.state.address,
				phone: this.state.phone,
			 });
 		 }	else {
 			 this.validator.showMessages();
			 console.log(this.validator);
 			 this.forceUpdate();
 		 }
 	 }

 	submitEmail() {
		let {email, confirmEmail} = this.validator.fields;
 		if (email && confirmEmail) {
			this.props.emailEdit(this.state.newEmail);
 		 }	else {
 			 this.validator.showMessages();
 			 // rerender to show messages for the first time
 			 this.forceUpdate();
 		 }
 	 }

 	submitPassword() {
		let {password, newPassword, confirmPassword} = this.validator.fields;
 		if (password && newPassword && confirmPassword) {
			this.props.passwordEdit({password:this.state.password, newPassword:this.state.newPassword});
 		 } else {
 			 this.validator.showMessages();
			 console.log(this.validator);
 			 // rerender to show messages for the first time
 			 this.forceUpdate();
 		 }
 	 }

  	render() {
    	return ( <Jumbotron>
	  	<h2>Contact information</h2>
			<Form>
			<FormGroup bsSize="small">
					<Col componentClass={ControlLabel} sm={2}>Name</Col>
					<input className="form-control" value={this.state.name} onChange={this.setName} />
					{this.validator.message('name', this.state.name, 'alpha', 'text-danger')}
			</FormGroup>
			<FormGroup bsSize="small">
				<Col componentClass={ControlLabel} sm={2}>Surname</Col>
				<input className="form-control" value={this.state.surname} onChange={this.setSurname} />
				{this.validator.message('surname', this.state.surname, 'alpha', 'text-danger')}
			</FormGroup>
			<FormGroup bsSize="small">
				<Col componentClass={ControlLabel} sm={2}>Company</Col>
				<input className="form-control" value={this.state.company} onChange={this.setCompany} />
				{this.validator.message('company', this.state.company, 'alpha_num', 'text-danger')}
			</FormGroup>
			<FormGroup bsSize="small">
				<Col componentClass={ControlLabel} sm={2}>Phone</Col>
				<input className="form-control" value={this.state.phone} onChange={this.setPhone} />
				{this.validator.message('phone', this.state.phone, 'phone', 'text-danger')}
			</FormGroup>
			<FormGroup bsSize="small">
				<Col componentClass={ControlLabel} sm={2}>Address</Col>
				<input className="form-control" value={this.state.address} onChange={this.setAddress} />
				{this.validator.message('address', this.state.address, 'alpha_num_dash', 'text-danger')}
			</FormGroup>

			<FormGroup bsSize="small">
						 <Button bsStyle="default" onClick={this.submitInfo} >Update Info</Button>
			</FormGroup>
		</Form>
		<h2>Change email</h2>
			<Form>
				<FormGroup bsSize="small">
					<Col componentClass={ControlLabel} sm={2}>New Email Address</Col>
					<input className="form-control" value={this.state.newEmail} onChange={this.setNewEmail} />
					{this.validator.message('email', this.state.newEmail, 'required|email', 'text-danger')}
				</FormGroup>
				<FormGroup bsSize="small">
					<Col componentClass={ControlLabel} sm={2}>Confirm New Email Address</Col>
					<input className="form-control" value={this.state.emailConfirm} onChange={this.setEmailConfirm} />
					{this.validator.message('confirmEmail', this.state.emailConfirm, 'required|email|email_confirm', 'text-danger')}
				</FormGroup>
			<FormGroup bsSize="small">
						 <Button bsStyle="default" onClick={this.submitEmail}>Update Email Address</Button>
			</FormGroup>
		</Form>

		<h2>Change password</h2>
			<Form>
			<FormGroup bsSize="small">
				<Col componentClass={ControlLabel} sm={2}>Current Password</Col>
				<input  className="form-control" type="password" value={this.state.password} onChange={this.setPassword} />
				{this.validator.message('password', this.state.password, 'min:8|alpha_num', 'text-danger')}
			</FormGroup>
			<FormGroup bsSize="small">
				<Col componentClass={ControlLabel} sm={2}>New Password</Col>
				<input  className="form-control" type="password" value={this.state.newPassword} onChange={this.setNewPassword} />
				{this.validator.message('newPassword', this.state.newPassword, 'min:8|alpha_num', 'text-danger')}
			</FormGroup>
			<FormGroup bsSize="small">
				<Col componentClass={ControlLabel} sm={2}>Confirm New Password</Col>
				<input  className="form-control" type="password" value={this.state.passwordConfirm} onChange={this.setPasswordConfirm} />
				{this.validator.message('confirmPassword', this.state.passwordConfirm, 'required|min:8|password_confirm', 'text-danger')}
			</FormGroup>
			<FormGroup bsSize="small">
						 <Button bsStyle="default" onClick={this.submitPassword}>Update Password</Button>
			</FormGroup>
		</Form>
	</Jumbotron>
		)

}
}
