import React from 'react'
import { Thumbnail } from 'react-bootstrap';

const AssetInfo =  ({ asset }) =>
	(	  <Thumbnail src={ asset.photo }  alt="242x200">
        <h3>{ asset.name }</h3>
				<p className='text-muted'>Id: {asset.id}</p>
				<b>Categories: </b><i>{ asset.categories.join(',')}</i>
				<br/>
				<br/>
				<h4>Descriptoin</h4>
				<p>{ asset.info }</p>
					<a href={ asset.doc } download>
						Documentation
					<img className="erase_margin" src={require("../../img/doc.png")} alt="documentation"></img>
				</a>
      </Thumbnail>
	)

export default AssetInfo;
