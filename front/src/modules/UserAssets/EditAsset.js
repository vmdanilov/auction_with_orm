import React from 'react'
import {  Form, FormGroup, Row, Col, Button, ControlLabel, FormControl, Image  } from 'react-bootstrap';
import Chips from 'react-chips'
import SimpleReactValidator from "simple-react-validator";

class EditAsset extends React.Component {

  constructor(props) {
    super()
    this.state = props.asset;
    this.validator = new SimpleReactValidator();

    this.photoHandler = this.photoHandler.bind(this);
    this.docHandler = this.docHandler.bind(this);
    this.changeChips = this.changeChips.bind(this);
    this.setName = this.setName.bind(this);
    this.setInfo = this.setInfo.bind(this);
    this.submitInfo = this.submitInfo.bind(this);
  }

  changeChips = categories => {
    this.setState({ categories });
  }

  photoHandler({target}) {
    var reader = new FileReader();
    reader.onload = e => {
      this.setState({
        photo: e.target.result
      })
   }
    reader.readAsDataURL(target.files[0])
  }

  docHandler({target}) {
    var reader = new FileReader();
    reader.onload = e => {
      this.setState({
        doc: e.target.result
      })
   }
    reader.readAsDataURL(target.files[0])
  }

  submitInfo() {
   if (this.validator.allValid()) {
     const data = new FormData();
     const {name, info, categories} = this.state;
     const about = JSON.stringify({name, info, categories});

     data.append('photo', this.photoFile.files[0]);
     data.append('doc', this.docFile.files[0]);
     data.append('info', about );

     this.props.editAsset(this.state.id,data);
    }	else {
      this.validator.showMessages();
      this.forceUpdate();
    }
  }

  setName(event) { this.setState({ name: event.target.value }) }
  setInfo(event) { this.setState({ info: event.target.value }) }

  render() {
  return  ( <Form>
    <FormGroup>
      <ControlLabel>Name</ControlLabel>
      <FormControl type="text" placeholder="" value={this.state.name} onChange={this.setName}/>
      {this.validator.message('name', this.state.name, 'alpha|required', 'text-danger')}
    </FormGroup>
    <FormGroup>
      <ControlLabel>Category</ControlLabel>
        <Chips
          placeholder="communication, routing.."
          value={this.state.categories}
          onChange={this.changeChips}
          suggestions={['communication', 'power', 'routing']}
        />
    </FormGroup>
    <FormGroup>
      <Row>
      <Col xs={6} md={6}>
        <ControlLabel>Description</ControlLabel>
        <FormControl componentClass="textarea" rows="5" value={this.state.info} onChange={this.setInfo} />
        {this.validator.message('info', this.state.info, 'min:20|max:250|required', 'text-danger')}
        <br/>
        <ControlLabel>Photo</ControlLabel>
        <input type="file" ref={(ref) => { this.photoFile = ref; }}  onChange={this.photoHandler} />
        {this.validator.message('photo', this.state.photo, 'required', 'text-danger')}
        <br/>
        <ControlLabel>Documentation</ControlLabel>
        <input type="file" ref={(ref) => { this.docFile = ref; }}  onChange={this.docHandler}/>
      </Col>
      <Col xs={6} md={6}>
        <Image src={this.state.photo} thumbnail />
      </Col>
    </Row>
    </FormGroup>
    <FormGroup>
           <Button
             bsStyle="default" onClick={this.submitInfo}
             disabled={this.state.auction && new Date(this.state.auction.startDate ) <= new Date()}
             block>Submit</Button>
    </FormGroup>
    </Form>
  	)
  }
}

export default EditAsset
