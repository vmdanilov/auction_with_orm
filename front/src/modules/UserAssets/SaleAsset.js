import React from 'react'
import { Button, Row, Col, ControlLabel, Thumbnail} from 'react-bootstrap';
import ReactAutocomplete from 'react-autocomplete'
import CustomDatePicker from '../DatePicker'
import moment from 'moment';

export default class SaleAsset extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.setStartDate = this.setStartDate.bind(this);
    this.setEndDate = this.setEndDate.bind(this);

    this.setStartBid = this.setStartBid.bind(this);
    this.setMinStep = this.setMinStep.bind(this);

    this.submitSale = this.submitSale.bind(this);

    this.initialState = {
      value: '',
      photo: '',
      info: '',
      LotId: null,
      startBid: 0,
      minStep: 0,
      startDate: moment()._d,
      endDate: moment()._d,
    };
    this.state = this.initialState;
  }

  setStartDate(date) { this.setState({startDate: date}) }
  setEndDate(date) { this.setState({endDate: date}) }

  setStartBid({target}) { this.setState({startBid: target.value}) }
  setMinStep({target}) { this.setState({minStep: target.value}) }

  submitSale() {
    let { startDate, endDate, LotId } = this.state;
    this.props.onSale({
      startBid: +this.state.startBid,
      minStep: +this.state.minStep,
      startDate, endDate, LotId });
      console.log(this.initialState);
    this.setState(this.initialState)
  }


  render() {
    return (
      <div>
        <h3>Sale Lot</h3>
        <Row>
        <Col xs={6} md={4}>
          <ControlLabel>Choose lot to sale</ControlLabel>
          <ReactAutocomplete
            items={this.props.assets.filter (asset => !asset.auction)}
            getItemValue={item => item.name}
            renderItem={(item, highlighted) =>
              <div
                key={item.id}
                style={{ backgroundColor: highlighted ? '#eee' : 'transparent' }}
              >
                {item.name}
              </div>
            }
            value={this.state.value}

            onSelect={(value, item) => {
              this.setState({
                 value,
                 photo: item.photo,
                 info: item.info,
                 LotId: item.id
               })
            }}
            inputProps={{ style: {positon:'relative', padding: '5px', borderRadius: '5px', display:'block'}}}
            wrapperStyle={{ width:  '100%'   }}
          />
        <br></br>
        <ControlLabel>Start bid</ControlLabel>
        <input type="text" className="money_input" value={this.state.startBid} onChange={this.setStartBid} placeholder=""/>$
        <br></br>
        <ControlLabel>Min step</ControlLabel>
        <input type="text" className="money_input" value={this.state.minStep}
           onChange={this.setMinStep} placeholder=""/>$
        <br></br>
        <br></br>
          <ControlLabel>Auction start date</ControlLabel>
          <CustomDatePicker returnDate={ this.setStartDate }></CustomDatePicker>
          <ControlLabel>Auction end date</ControlLabel>
          <CustomDatePicker returnDate={ this.setEndDate } ></CustomDatePicker>
          <br></br>
          <Button block onClick={this.submitSale}>Sale</Button>
        </Col>
        <Col xs={6} md={8}>
          <Thumbnail   alt="242x200"  src={this.state.photo}>
            <h3 className='text-uppercase'>{this.state.value}</h3>
            <p className='text-info'><small>{this.state.info}</small></p>
          </Thumbnail>
        </Col>
      </Row>
      </div>
    );
  }
}
