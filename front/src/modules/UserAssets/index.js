import { connect } from 'react-redux'
import { gotAllAssets, editAsset, deleteAsset, saleAsset, createAsset } from '../../actions/user'
import AssetsControl from './AssetsControl'
import api from '../api';

const mapStateToProps = (state, ownProps) => ({
	assets: state.user.assets
})

const mapDispatchToProps = (dispatch, ownProps) => ({
	getAllUserAssets: () => {
		console.log(arguments);
		api.get(`/user/lots/all`)
			.then(res => {
				dispatch( gotAllAssets(res.data.lots) );
			})
			.catch(err => alert(err))
	},

	createAsset : (data) => {
		api.post('/user/lots/create', data)
			.then(res => {
        console.log(res);
				dispatch(createAsset(res.data.Lot));
        alert('success')
			})
			.catch(err => console.log(err))
	},

	editAsset : (id, data) => {
		api.post(`/user/lots/edit/${id}`, data)
			.then(res => {
				console.log(res);
				dispatch(editAsset(res.data.lot));
				alert('success')
			})
			.catch(err => console.log(err))
	},

	deleteAsset: (id) => {
		api.delete(`/user/lots/delete/${id}`)
			.then(res => {
				dispatch( deleteAsset(id) );
			})
			.catch(err => console.log(err.response.data))
	},

	saleAsset: (asset) => {
		api.post(`/user/lots/sale`, asset)
			.then(res => {
				res.data.auction.Users = [];
				dispatch( saleAsset(res.data.auction) );
			})
			.catch(err => console.log(err))
	}
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AssetsControl)
