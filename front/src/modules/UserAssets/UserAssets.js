import React from 'react'
import { Table, Button, Modal, Nav, NavItem, Label} from 'react-bootstrap';
import AssetInfo from './AssetInfo'
import EditAsset from './EditAsset'

export default class UserAssets extends React.Component {
  constructor (props) {
    super(props)

		this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.toogleWinds = this.toogleWinds.bind(this);

    this.state = {
			show: false,
			assetId: null,
			wind: 'info'

    };
  }

  componentWillMount () {
		this.props.getAllUserAssets();
  }

	handleClose() {
    this.setState({ show: false });
  }

  handleShow(id, wind) {
    this.setState({ show: true, wind, assetId: id});
  }

  toogleWinds(key) {
		this.setState({ wind:key })
  }

  render() {
		let { assets } = this.props

    return ( <div>
		<Table responsive>
	  <thead>
	    <tr>
	      <th>id</th>
	      <th>Name</th>
	      <th>Categories</th>
	      <th>Info</th>
	      <th>Edit</th>
	      <th>Delete</th>
	    </tr>
	  </thead>
	  <tbody>
	    {assets.map((lot, i) =>
				( <tr key={i}>
		      <td>{lot.id}</td>
		      <td>{lot.name}</td>
		      <td>{lot.categories.join(',')}</td>
		      <td><Button bsStyle='info' bsSize='small' onClick={() => this.handleShow(lot.id, 'info')}>Show info</Button></td>
		      <td><Button bsStyle='warning' bsSize='small'
             onClick={() => this.handleShow(lot.id, 'edit')}
             disabled={lot.auction && new Date(lot.auction.startDate ) <= new Date()}
             >Edit</Button></td>
		      <td>
						<Button bsStyle='danger' bsSize='small'
						 onClick={ () => this.props.deleteAsset(lot.id) }
             disabled={lot.auction && new Date(lot.auction.startDate ) <= new Date()}>
						 Delete
					 </Button>

		      </td>
          {lot.auction ? <td><Label>Sales</Label></td>: null}
		    </tr>
				))}
	  </tbody>
	</Table>
		<Modal show={this.state.show} onHide={this.handleClose}>
			<Modal.Header closeButton>
				<Nav bsStyle="pills" activeKey={this.state.wind} onSelect={ this.toogleWinds }>
					<NavItem eventKey={'info'}>
						Asset Info
					</NavItem>
					<NavItem eventKey={'edit'} title="Item" >
						Edit Asset
					</NavItem>
				</Nav>
			</Modal.Header>
			<Modal.Body>
				{this.state.wind === 'info' ?
					 <AssetInfo asset={assets.filter( elem => (elem.id === this.state.assetId))[0]}></AssetInfo> :
				 	 <EditAsset
             asset={assets.filter( elem => (elem.id === this.state.assetId))[0] }
             editAsset={this.props.editAsset} ></EditAsset>}
			</Modal.Body>
		</Modal>
	</div>
		)
  }
}
