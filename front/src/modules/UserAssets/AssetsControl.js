import React from 'react'
import { Jumbotron, Tab, Row, Col, Nav, NavItem, } from 'react-bootstrap';
import UserAssets from './UserAssets';
import CreateAssetForm from './CreateAssetForm';
import SaleAsset from './SaleAsset';

const AssetsControl =  ({assets, getAllUserAssets, editAsset, deleteAsset, saleAsset, createAsset}) =>
	( <div className='tall'>
		<Jumbotron>
		<Tab.Container id="left-tabs-example" defaultActiveKey="userAssets">
			<Row className="clearfix">
				<Col sm={4}>
					<Nav bsStyle="pills" >
						<NavItem eventKey="userAssets">My Assets</NavItem>
						<NavItem eventKey="create">Add Asset</NavItem>
						<NavItem eventKey="saleAsset">Sale Asset</NavItem>
					</Nav>
				</Col>
				<Col sm={8}>
					<Tab.Content animation>
						<Tab.Pane eventKey="userAssets">
							<UserAssets
							assets={assets}
							getAllUserAssets= {getAllUserAssets}
							editAsset={editAsset}
							deleteAsset={deleteAsset}
							 />
						</Tab.Pane>
						<Tab.Pane eventKey="create"><CreateAssetForm onCreate={createAsset}></CreateAssetForm></Tab.Pane>
						<Tab.Pane eventKey="saleAsset">
							<SaleAsset
							 assets={assets}
							 onSale={saleAsset}
							 ></SaleAsset></Tab.Pane>
					</Tab.Content>
				</Col>
			</Row>
		</Tab.Container>
		</Jumbotron>
		</div>
	)
export default AssetsControl;
