import React, { Component } from 'react';
import Chips from 'react-chips'

class CategoyPicker extends Component {

  constructor(props) {
    super(props);
    this.state = {
      chips: []
    }
  }

  componentWillMount() {
    this.setState({
      chips:this.props.chips
    })
  }

  changeChips = chips => {
    this.setState({ chips });
  }

  render() {
    return (
      <div>
        <Chips
					placeholder="communication, routing.."
          value={this.state.chips}
          onChange={this.changeChips}
          suggestions={this.props.categories}
        />
      </div>
    );
  }
}

export default CategoyPicker
