import React from 'react'
import PropTypes from 'prop-types'
import { Grid, Row, Col, Button, Thumbnail, Well } from 'react-bootstrap';
import { withRouter } from 'react-router-dom'

let getStatus = (start, end) => {
	let composer = new Date().getDate();
	start = new Date(start).getDate();
	end = new Date(end).getDate();

	if (composer >= start && composer <= end) return `"in process"`;
	if (composer > end) return `"ended"`;
	if (composer < start) return `"will begin after ${start-composer} days"`;
}

const UserAuctions =  ({ auctions, history }) =>
	(<Grid>
		{auctions.map((auction, i) => {
			return (<Well key={i}>
			<Row>
				<Col xs={6} md={3}>
					<Thumbnail src={auction.Lot.photo}  alt="242x200">
						<h3>{auction.Lot.name}</h3>
					</Thumbnail>
				</Col>
				<Col xs={6} md={4}>
					<p>Start Date: {auction.startDate.slice(0,10)}</p>
					<p>End Date: {auction.endDate.slice(0,10)}</p>
					<p>Start Bid: {auction.startBid}$</p>
					<p>Min Bid: {auction.minStep}$</p>
				</Col>
				<Col xs={6} md={3}>
					<p className="text-info">Staus: <i>{getStatus(auction.startDate, auction.endDate)}</i></p>
				</Col>
				<Col xs={6} md={2}>
					<Button bsStyle='info' onClick={() => history.push(`/auction/${auction.id}`)} bsSize='large' block>Join</Button>
				</Col>
			</Row>
		</Well>
	)})
	}
	</Grid>
	)

UserAuctions.propTypes = {
  auctions: PropTypes.array.isRequired
}

export default withRouter(UserAuctions);
