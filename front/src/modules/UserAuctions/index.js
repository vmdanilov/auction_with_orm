import React from 'react'
import { Jumbotron, Tab, Row, Nav, NavItem, PageHeader } from 'react-bootstrap';
import UserAuctions from './UserAuctions'
import { connect } from 'react-redux'
import { gotAllAuctions, gotAllRegistredAuctions } from '../../actions/user'
import api from '../api';

class Auctions extends React.Component{

	constructor(props) {
		super();
	}

	componentWillMount() {
		this.props.getAllUserAuctions();
		this.props.getAllRegistredAuctions();
	}

	render() {
		let {userAuctions, registredAuctions} = this.props;
		return ( <div className='tall'>
			<Jumbotron>
				<Tab.Container id="left-tabs-example" defaultActiveKey="UserAuctions">
					<Row className="clearfix">
						<Nav bsStyle="pills" justified>
							<NavItem eventKey="UserAuctions">My Auctions</NavItem>
							<NavItem eventKey="create">I Registred</NavItem>
						</Nav>
						<Tab.Content animation>
							<Tab.Pane eventKey="UserAuctions">
								<PageHeader>
  								<small>Auctions</small>
								</PageHeader>
									<UserAuctions auctions={userAuctions}></UserAuctions>
								</Tab.Pane>
								<Tab.Pane eventKey="create">
									<PageHeader>
  								<small>Registered</small>
								</PageHeader>
								<UserAuctions auctions={registredAuctions}></UserAuctions>
							</Tab.Pane>
						</Tab.Content>
					</Row>
				</Tab.Container>
			</Jumbotron>
			</div>
	)
}
}


const mapStateToProps = (state, ownProps) => ({
	userAuctions: state.user.auctions,
	registredAuctions: state.user.regAuctions,
})

const mapDispatchToProps = (dispatch, ownProps) => ({
	getAllUserAuctions: () => {
		api.get('/user/auctions/all')
			.then(res => {
				dispatch(gotAllAuctions(res.data.auctions));
			})
			.catch(err => console.log(err))
	},
	getAllRegistredAuctions: () => {
		api.get('/user/auctions/registered')
			.then(res => {
				console.log(res);
				dispatch(gotAllRegistredAuctions(res.data.auctions));
			})
			.catch(err => console.log(err))
	},
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Auctions)
