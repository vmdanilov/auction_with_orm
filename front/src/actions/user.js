export const fetchLogin = (user, token) => ({
	type: 'FETCH_LOGIN',
	payload: user,
	token
})

export const logOut = () => ({
	type: 'LOG_OUT'
})

export const editProfile = (info) => ({
	type: 'EDIT_PROFILE',
	payload: info
})

export const gotAllAssets = (assets) => ({
	type: 'GOT_ALL_ASSETS',
	payload: assets
})

export const deleteAsset = (id) => ({
	type: 'DELETE_ASSET',
	payload: id
})

export const createAsset = (asset) => ({
	type: 'CREATE_ASSET',
	payload: asset
})

export const editAsset = (asset) => ({
	type: 'EDIT_ASSET',
	payload: asset
})

export const saleAsset = (auction) => ({
	type: 'SALE_ASSET',
	payload: auction
})
export const gotAllAuctions = (auction) => ({
	type: 'GOT_ALL_USER_AUCTIONS',
	payload: auction
})
export const gotAllRegistredAuctions = (auction) => ({
	type: 'GOT_ALL_REG_AUCTIONS',
	payload: auction
})
