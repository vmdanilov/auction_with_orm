export const gotAllAuctions = (auctions, count) => ({
	type: 'GOT_ALL_AUCTIONS',
	payload: auctions,
	count
})

export const userRegistered = (auctionId, user) => ({
	type: 'USER_REGISTERED',
	payload: auctionId,
	user
})

export const userCancelReg = (auctionId, user) => ({
	type: 'USER_CANCEL_REG',
	payload: auctionId,
	user
})
