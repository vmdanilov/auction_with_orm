import { combineReducers } from 'redux'
import user from './user'
import main from './main'

export default combineReducers({
  user,
  main
})
