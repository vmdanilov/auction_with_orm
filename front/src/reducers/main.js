const initialState = {
	allAuctions: [],
	count: 1,
	filter: {}
};

const mainReducer = (state = initialState, action) => {

	switch(action.type) {
		case 'GOT_ALL_AUCTIONS':
			return {...state, allAuctions: action.payload, count: action.count};
		case 'USER_REGISTERED':
			return {...state, allAuctions: state.allAuctions.map(auct => {
				if (auct.id === action.payload) auct.Users.push(action.user)
				return auct;
			})};
		case 'USER_CANCEL_REG':
			return {...state, allAuctions: state.allAuctions.map(auct => {
				if (auct.id === action.payload) auct.Users = auct.Users.filter(user => user !== action.user);
				return auct;
			})};
		default:
			return state;
	}
}

export default mainReducer
