const initialState = {
	token: '',
	info: 	 {},
	assets: [],
	auctions: [],
	regAuctions: [],
	error: 	 null
};

const userReducer = (state = initialState, action) => {
	switch(action.type) {
		case 'FETCH_LOGIN':
			return { ...state, info: action.payload, token: action.token };
		case 'LOG_OUT':
			return initialState;
		case 'EDIT_PROFILE':
			return { ...state, info: action.payload};
		case 'GOT_ALL_ASSETS':
			return {...state, assets: action.payload};
		case 'DELETE_ASSET':
			return {...state, assets: state.assets.filter(elem => elem.id !== action.payload)};
		case 'CREATE_ASSET':
			return {...state, assets: [...state.assets, action.payload ] };
		case 'EDIT_ASSET':
			return {...state, assets: state.assets.map(asset => {
				return asset.id === action.payload.id ?  action.payload : asset
			}) };
		case 'SALE_ASSET':
			return {
				...state,
				 auctions: [...state.auctions, action.payload ],
				 assets: state.assets.map(asset => {
					 if (asset.id === action.payload.LotId) asset.auction = action.payload;
					 return asset;
				 })
				  };
		case 'GOT_ALL_USER_AUCTIONS':
			return {...state, auctions: action.payload};
		case 'GOT_ALL_REG_AUCTIONS':
			return {...state, regAuctions: action.payload};

		default:
			return state;
	}
}

export default userReducer
