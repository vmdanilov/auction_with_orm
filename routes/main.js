const express = require('express');
const router = express.Router();
const auth = require("../auth.js")();
const auctionController = require('../controllers/auctionController');

router.get('/auctions/all', auctionController.getAllAuctions)
router.get('/auctions/show/:id', auctionController.getAuction);
router.get('/auctions/find', auctionController.findAuction);
router.post('/auctions/register/:id', auth.authenticate(), auctionController.registerUser);
router.post('/auctions/register/cancel/:id', auth.authenticate(), auctionController.cancelUserRegistration);

module.exports = router;
