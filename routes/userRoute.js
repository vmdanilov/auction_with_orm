const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const lotController = require('../controllers/lotController');
const auctionController = require('../controllers/auctionController');
const auth = require("../auth.js")();


router.get('/profile/:id', auth.authenticate(), userController.getOneUser);
router.post('/profile/edit', auth.authenticate(), userController.editUser);
router.post('/profile/edit/password', auth.authenticate(), userController.editUserPassword);

router.get('/lots/all', auth.authenticate(), lotController.getAllUserLots);
router.post('/lots/create', auth.authenticate(), lotController.createLot);
router.post('/lots/edit/:id', auth.authenticate(),  lotController.editLot);
router.delete('/lots/delete/:id', lotController.deleteLot);
router.post('/lots/sale', auth.authenticate(), auctionController.createAuction);

router.get('/auctions/all', auth.authenticate(), auctionController.getAllUserAuctions);
router.get('/auctions/registered', auth.authenticate(), auctionController.getAllUserRegisteredAuctions);
router.post('/auctions/editAuction/:id', auth.authenticate(), auctionController.editAuction);
router.delete('/auctions/delete/:id', auth.authenticate(), auctionController.deleteAuction);

module.exports = router;
