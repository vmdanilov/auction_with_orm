const db = require('../models/index');
const User = require('../models').User;
const cfg = require("../auth_config.js");
const jwt = require("jwt-simple");

const express = require('express');
const router = express.Router();

module.exports = router.post("/login", function(req, res) {
    if (req.body.email && req.body.password) {
        let email = req.body.email;
        let password = req.body.password;

        User.findOne( { where: { email, password } })
        	.then(result => {
            if (result) {
        		let user = result.dataValues;
                let payload = {
                    id:   user.id,
                    role: user.role
                };

                let token = jwt.encode(payload, cfg.jwtSecret);
                let {id, email, name, surname, address, phone } = user;
                res.json({
                    token: token,
                    user: {id, email, name, surname, address, phone }
                });
            } else {
              res.status(401)
                .send('Invalid email or password')
            }
          })
      } else res.status(401)
          .send('Invalid email or password')

});
