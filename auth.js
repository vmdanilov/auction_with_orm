const passport = require("passport");
const passportJWT = require("passport-jwt");
const ExtractJwt = passportJWT.ExtractJwt;
const Strategy = passportJWT.Strategy;
const cfg = require("./auth_config.js");

const db = require('./models/index');
const User = db.sequelize.import("./models/user");

const params = {
    secretOrKey: cfg.jwtSecret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt")
};

module.exports = function() {
    const strategy = new Strategy(params, function(payload, done) {
				User.findById(payload.id)
					.then(result => {
						const user = result.dataValues;
						return user ? done(null, { id:user.id, role:user.role })
												: done(new Error("User not found"), null);
					})
        	.catch(err => console.log(err))
    });

    passport.use(strategy);
    
    return {
        initialize: function() {
            return passport.initialize();
        },
        authenticate: function() {
            return passport.authenticate("jwt", cfg.jwtSession);
        }
    };
};
