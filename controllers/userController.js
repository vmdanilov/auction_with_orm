const db = require('../models/index');
const User = require('../models').User
const Lot = require('../models').Lot
const Auction = require('../models').Auction
const ERRORS = require('./errors');
const jwt = require("jsonwebtoken");
const secret = require("../auth_config.js");

module.exports = {
	getOneUser: function(req, res) {
		User.findById(req.params.id, {
			include: [{
				model:Lot
			}]
		})
			.then(user => {
				res.send({ user:	{
					email: user.dataValues.email,
					name: user.dataValues.name,
					surname: user.dataValues.surname,
					phone: user.dataValues.phone,
					address: user.dataValues.address,
				} })
			})
			.catch(err => {
				res.status(400)
					.send({ message: err.message })
			})
	},

	getAllUser: function(req, res) {
		req.query ? User.findAll({where: req.query}) : User.findAll()
			.then(result => {
				let users = result.map(elem => elem.dataValues);
				res.send({data:users})
			})
			.catch(err => {
				res.status(400)
					.send({ message: err.message })
			})
	},

	createUser: function (req, res) {
		User.create(req.body)
			.then(info => {
				res.send({ message: 'user successfully registered', info});
			})
			.catch(err => {
				err.errors.forEach(e => { console.log(e.message)})
				res.status(400)
					.send({ message: err.errors.map( e =>  e.message )})
			})
	},

	editUser: function (req, res) {
		let id = jwt.verify(
	    req.headers.authorization.slice(4),
	    secret.jwtSecret
	  ).id

		User.findById(id)
			.then(result => {
				if (result) {
					result.updateAttributes(req.body)
						.then(result => {
							let {id, name, surname, phone, address, email, company} = result.dataValues;
							let user = {id, name, surname, phone, address, email, company}
							res.send({ user, message: 'successfully updated' })
						})
						.catch(err => {
							res.status(400)
								.send({ message: err.message });
						})
				} else res.status(400).send({ info: ERRORS.USER_EXISTING })
			})
			.catch(err => {
				res.status(400)
					.send({ message: err.message });
			})
	},

	editUserPassword: function (req, res) {
		let id = jwt.verify(
	    req.headers.authorization.slice(4),
	    secret.jwtSecret
	  ).id

		User.findById(id)
			.then(result => {
				if (result && result.password == req.body.password) {
					result.updateAttributes({password: req.body.newPassword})
						.then(result => {
							res.send({message: 'password has been changed' })
						})
						.catch(err => {
							res.status(400)
								.send({ message: err.message });
						})
				} else res.status(400).send('INCORECT PASSWORD OR USER INFO')
			})
			.catch(err => {
				res.status(400)
					.send({ message: err.message });
			})
	},

	deleteUser: function (req, res) {
		User.destroy({where: { id: req.params.id }})
			.then(enfo => {
				res.send('user has been deleted')
			})
			.catch(err => {
				res.status(400)
					.send({ message: err.message });
			})
	},
}
