const db = require('../models/index');
const sequelize = db.sequelize;
const User = require('../models').User
const Lot = require('../models').Lot
const Auction = require('../models').Auction
const moment = require('moment');

const secret = require("../auth_config.js");
const jwt = require("jsonwebtoken");

const ERRORS = require('./errors');

const auctionController = {
		getAllAuctions: (req, res) => {
			let condition;
			let limit = 8;
			let offset = (req.query.page -1) * limit;

			req.query.startDate ?
			condition = {
				startDate: {
					[sequelize.Op.gte]:  new Date(req.query.startDate)
				},
				endDate: {
					[sequelize.Op.lte]: new Date(req.query.endDate)
				}
			} : condition = {}

			Auction.findAll({
				include:[ {model:Lot	}, {model:User} ],
				offset,
				limit,
				where: condition
			})
				.then( result => {
					let auctions = 	result
						.filter(auct => auct.dataValues.Lot)
						.map(auct => {
							auct.dataValues.Users = auct.dataValues.Users.map(user => user.id);
							return auct.dataValues
						})
						Auction.count()
							.then(count => {
								res.send({
									auctions,
									count
								})
							})
				})
				.catch(err => res.status(500).send({message: err.message}))
		},

		getAuction: (req, res) => {
			const auctId = req.params.id;

			Auction.findById(auctId, {include:[
				{
					model:Lot,
				},
				{
					model:User,
				},
				]})
				.then(auct => {
					let auction = auct.dataValues;
					auction.Lot = auction.Lot.dataValues;
					auction.Users = auction.Users.map(user => user.id);
					res.send({ auction });
				})
		},

		findAuction: (req, res) => {
			Auction.findOne({
				include:[
					{ model:Lot,
						where: {name: {[sequelize.Op.like]:`%${req.query.name}%`}}
					},
					{ model:User },
					]
			})
				.then(auct => {
					if (auct) {
						res.send({ id: auct.dataValues.id });
					} else res.status(400).send({ message: 'nothing' })
				})
				.catch(err => res.status(400)
					.send({
						message: err.message
					}))
		},

		createAuction: (req, res) => {
			let { startBid, minStep, startDate, endDate, LotId} = req.body;
			startDate = new Date(startDate);
			endDate = new Date(endDate);

			if (startDate.getDate() >= moment()._d.getDate() && startDate.getTime() < endDate.getTime()) {
				Lot.findById(LotId)
					.then(lot => {
						if (!lot) res.status(400).send({message:ERRORS.LOT_EXISTING})
						Auction.create({startDate, endDate, startBid, minStep})
							.then(auction => {
								auction.setLot(lot)
									.then(result => {
										auction.dataValues.Lot = lot
										res.send({
											message: 'success',
											auction
										})
									})
							})
					})
					.catch(err => {
						res.status(400)
							.send({
								message: err.message
							});
					})
			} else {
				res.status(400)
					.send({
						message: 'incorect auction date'
					});
			}
		},

		editAuction: (req, res) => {
			const UserId = jwt.verify(
		    req.headers.authorization.slice(4),
		    secret.jwtSecret
		  	).id
			const auctId = req.params.id;

			Auction.findById(auctId, {
				include: [
					{	model: Lot }
				]
			})
				.then(auction => {
					if (auction.dataValues.Lot.dataValues.UserId != UserId ||
					auction.startDate.getTime() <= moment()._d.getTime()
				) res.status(400).send({message:'permisson denied'})

					auction.updateAttributes(req.body)
						.then(result => {
							res.send({
								message: 'success',
								auction: result
							})
						})
				})
				.catch(err => {
					res.status(400)
						.send({
							message: err.message
						});
					});
		},

		deleteAuction: (req, res) => {
			const userId = jwt.verify(
				req.headers.authorization.slice(4),
				secret.jwtSecret
			).id

			const auctId = req.params.id;

			Auction.findById(auctId, {
				include: [
					{	model: Lot }
				]
			})
				.then(auction => {
					if (auction.dataValues.Lot.dataValues.UserId != UserId ||
							auction.startDate.getTime() <= moment()._d.getTime()
						) res.status(400).send({message:'permisson denied'})

					auction.destroy()
						.then(info => {
							res.send({
								message: 'success',
								info
							})
						})
				 })
				 .catch(err => res.status(400)
					 .send({
						 message: err.message
					 }))
		},

		getAllUserAuctions: (req, res) => {
			const UserId = jwt.verify(
		    req.headers.authorization.slice(4),
		    secret.jwtSecret
		  	).id

				Auction.findAll({
					include:[
						{
							model: Lot,
							where:[{UserId}]
						},
						{model:User}
					 ]
				})
				.then( result => {
					let auctions = 	result
						.filter(auct => auct.dataValues.Lot)
						.map(auct => {
							auct.dataValues.Users = auct.dataValues.Users.map(user => user.id);
							return auct.dataValues;
						})

						res.send({
							auctions
						})
					})
				.catch(err => res.status(400).send({message:err.message}))
		},

		getAllUserRegisteredAuctions: (req, res) => {
			const UserId = jwt.verify(
		    req.headers.authorization.slice(4),
		    secret.jwtSecret
		  	).id

				Auction.findAll({
					include:[
						{	model: Lot },
						{
							model: User,
							where: [{id:UserId}]
						}
					 ]
				})
				.then( result => {
					let auctions = 	result
						.filter(auct => auct.dataValues.Lot)
						.map(auct => auct.dataValues)
						res.send({
							auctions
						})
					})
				.catch(err => res.status(400).send({message:err.message}))
		},

		registerUser: (req, res) => {
			const UserId = jwt.verify(
		    req.headers.authorization.slice(4),
		    secret.jwtSecret
		  	).id
			const auctId = req.params.id;

			Auction.findById(auctId)
				.then(auction => {
					User.findById(UserId)
						.then(user => {
							auction.addUser(user)
								.then(info => {
									res.send({message:'success', info})
								})
						})
				})
				.catch(err => res.status(400).send({message:err.message}))
		},

		cancelUserRegistration: (req, res) => {
			const UserId = jwt.verify(
				req.headers.authorization.slice(4),
				secret.jwtSecret
				).id
			const auctId = req.params.id;

			Auction.findById(auctId)
				.then(auction => {
					User.findById(UserId)
						.then(user => {
							auction.removeUser(user)
								.then(info => {
									res.send({message:'success', info})
								})
						})
				})
				.catch(err => res.status(400).send({message:err.message}))
		},

		checkDate: (req, res, next) => {
			const auctId = req.params.id;
			Auction.findById(auctId)
				.then(auct => {
					let end = new Date(auct.dataValues.endDate).getTime();
					let start = new Date(auct.dataValues.startDate).getTime();
					return (end <= moment()._d.getTime() || start >= moment()._d.getTime()) ?
					res.send({message: 'auction is not active'}) : next();
				})
		}

}
module.exports = auctionController
