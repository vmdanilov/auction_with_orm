const ERRORS = {
	USER_EXISTING: {
		title: 'USER_EXISTING_ERROR',
		info:  'user with such params does not exist'
	},
	LOT_EXISTING: {
		title: 'LOT_EXISTING_ERROR',
		info:  'lot with such params does not exist'
	},

	// USER_EXISTING: {
	// 	title:	 'USER_EXISTING_ERROR',
	// 	message: 'user with such params does not exist'
	// }
}

module.exports = ERRORS;
