const db = require('../models/index');
const sequelize = db.sequelize;
const User = require('../models').User
const Category = require('../models').Category
const Lot = require('../models').Lot
const Auction = require('../models').Auction


const secret = require("../auth_config.js");
const jwt = require("jsonwebtoken");

const ERRORS = require('./errors');
const SERVER_DOMEN = 'http://localhost:8000';

const lotController = {
	getOneLot: function(req, res) {
		Lot.findById(req.params.id)
			.then(lot => {
				res.send({ data:	lot.dataValues })
			})
			.catch(err => {
				db.sequelize.close();
				res.status(400)
					.send({ message: err.message })
			})
	},

	getAllLots: function(req, res) {
		Lot.findAll({where: req.query})
			.then(result => {
				let lots = result.map(elem => elem.dataValues);
				// db.sequelize.close();
				res.send({data:lots})
			})
			.catch(err => {
				// db.sequelize.close();
				res.status(400)
					.send({ message: err.message })
			})
	},

	getAllUserLots(req, res, next) {
		const userId = jwt.verify(
	    req.headers.authorization.slice(4),
	    secret.jwtSecret
	  	).id

		Lot.findAll({
			include:[{
				model:User,
				where:[{id:userId}]
			},
			{ model:Category },
			{	model:Auction	}
			]
		})
		.then(lots => {
			// db.sequelize.close();
			res.send({
				lots: lots.map(lot => {
					return {
					id: lot.dataValues.id,
					name: lot.dataValues.name,
					photo: lot.dataValues.photo,
					info: lot.dataValues.info,
					auction: lot.dataValues.Auction,
					categories: lot.dataValues.Categories.map(category => category.name)
				}
				})
			})
		})
		.catch(err => {
			// db.sequelize.close();
			res.status(400)
				.send({ message: err.message })
		})
	},

	uploadFiles: function(files) {
		return new Promise( (res, rej) => {
			if (!files) return res({});
			let paths = {};
			for (let file in files) {
				let id = new Date().getTime();
				files[file].mv(`${__dirname}/../public/${file}/${id}-${files[file].name}`, err => {
					if (err) return rej(err);
					paths[file] = `${SERVER_DOMEN}/public/${file}/${id}-${files[file].name}`;

					if (Object.keys(paths).length == Object.keys(files).length) return res(paths)
				})
				}
		})
	},

	createLot: function(req, res) {
		const UserId = jwt.verify(
			req.headers.authorization.slice(4),
			secret.jwtSecret
			).id

		lotController.uploadFiles(req.files)
			.then(paths => {
				let newLot = {...(JSON.parse(req.body.info)), ...paths, UserId}
				Lot.create(newLot)
					.then(lot => {
						Category.findAll({
							where:{
								name: {[sequelize.Op.in]: newLot.categories}
							}
						})
						.then(cats => {
							lot.addCategories(cats)
								.then(result => {
									let categories = cats.map(cat => cat.dataValues.name);
									let data = {...lot.dataValues, categories}

									res.send({
										message: 'success',
										Lot: data
									})
								})
						})
						})
					.catch(err => {
						res.status(400)
							.send({ message: err.message })
						});
			})
	},

	editLot: function(req, res) {
		const UserId = jwt.verify(
			req.headers.authorization.slice(4),
			secret.jwtSecret
			).id
		lotController.uploadFiles(req.files)
			.then(paths => {
				let update = {...(JSON.parse(req.body.info)), ...paths}
				Lot.findOne({
					where: {
						[sequelize.Op.and]: [{ id: req.params.id	}, { UserId: UserId }]
					},
					include: [{model:Auction}]
				})
					.then(lot => {
						Category.findAll({
							where:{
								name: {[sequelize.Op.in]: update.categories}
							}
						})
						.then(cats => {
							lot.setCategories([])
								.then((reset) => {
									lot.addCategories(cats)
										.then(result => {
											lot.updateAttributes(update)
												.then(result => {
													let categories = cats.map(cat => cat.dataValues.name);
													let data = {...result.dataValues, categories}
													res.send({
														message: 'success',
														lot: data
													})
												})
										})
								})

						})
					})
					.catch(err => {
						res.status(400)
							.send({
								message: err.message
							});
						})
			})
	},

	deleteLot: function (req, res) {
		const userId = jwt.verify(
			req.headers.authorization.slice(4),
			secret.jwtSecret
		).id

		Lot.destroy({
			where: {
				[db.Sequelize.Op.and]: [{id:req.params.id}, {UserId:userId}]
			}
		})
			.then(info => {
				if (info == 1) {
					res.send({message: 'lot has been successfully removed'})
				} else res.status(400)
								.send({
									message: info
								});
			})
			.catch(err => {
				res.status(400)
					.send({ message: err.message });
			})
	}

}
module.exports = lotController
