const User = require('../models').User
const Auction = require('../models').Auction
const Lot = require('../models').Lot
const Category = require('../models').Category
const Bet = require('../models').Bet
const db = require('../models')
const sequelize = db.sequelize;

const moment = require('moment');

const user = {
	email:'vlad@vlad.com',
	password:'qwerty12'
}
const lot = {
	name: 'phone'
}
const auction = {
	startDate: new Date(),
	endDate: new Date().setMonth(6),
	minStep: 10,
}

// User.findById(1)
// 	.then(user => {
// 		Lot.create({name: 'laptop'})
// 			.then(lot => {
// 				Category.findById(1)
// 					.then(cat => {
// 						lot.addCategory(cat);
// 						user.addLot(lot);
// 					})
// 			})
// 	})
// Lot.create({name:'newStuff'})
// 	.then(lot => {
// 		Category.findById(3)
// 			.then(cat => {
// 				lot.addCategory(cat)
// 					.then(res => console.log(lot))
// 			})
// 	})
// Lot.findById(4)
// 	.then(lot => {
// 		Category.findById(1)
// 			.then(cat => {
// 				lot.addCategory(cat);
// 			})
// 	})

//SELECT A WITH CATEGORISES
// select a.*
// from auctionDev.Auctions a
// inner join auctionDev.lotCategory lc on a.LotId = lc.LotId
// group by a.LotId, a.id
// having group_concat(distinct lc.CategoryId) = '1,2'

// sequelize.query("select a.* \
// 										f from auctionDev.Auctions a \
// 										inner join auctionDev.lotCategory lc on a.LotId = lc.LotId\
// 									 	group by a.LotId, a.id \
// 										having group_concat(distinct lc.CategoryId) = :categories",
//
//   {  model: Lot, replacements: { categories: [1].join(',') }, type: sequelize.QueryTypes.SELECT }
// ).then(lots => {
//   lots.forEach(lot => console.log(lot.dataValues))
// })

// let arr = ['1','2'].map((elem, i) => {
// 	return {
// 		id: elem
// 	}
// })
//
// console.log(arr);
// //
// Lot.findAll({
// 	include:[{
// 		model:Category,
// 		where: {
// 			[db.Sequelize.Op.or]: [{id:1}, {id:2}]
// 		}
// 	}]
// })
// .then(lots => {
// 	// lots = lots.filter(elem => {
// 	// 	// console.log(elem.dataValues.Categories.length)
// 	// 	return elem.dataValues.Categories.length >= arr.length
// 	// })
// 	// .reduce((init, elem) => {
// 	// 	 init.push({
// 	// 		userId:  elem.dataValues.iserId,
// 	// 		lotId:   elem.dataValues.id,
// 	// 		lotName: elem.dataValues.name,
// 	// 		Categories: elem.dataValues.Categories.reduce((init, elem) => {
// 	// 			init.catIds.push(elem.dataValues.id)
// 	// 			init.catNames.push(elem.dataValues.name)
// 	// 			return init;
// 	// 		}, {catIds:[], catNames:[]})
// 	// 	})
// 	// 	return init;
//   //
// 	// }, [])
// lots.forEach(elem => {
// 	console.log('lot id: ',elem.dataValues.id);
// 	elem.dataValues.Categories.forEach(elem => {
// 		console.log('category id: ', elem.dataValues.id);
// 	})
// 	console.log('--------------------');
// })
// })

// Lot.findAll({
// 	where: {
// 	       id: {
// 					 [db.Sequelize.Op.and]: [1]
// 				 }
//
// 	  }
// })
// .then(res => console.log(res))

// Lot.findAll(
// 	{
// 	where: {id:1},
// 	include:[
// 		{
// 		model: User
// 		},
//
// 		{
// 			model:Auction
// 		}
// 	]
// })
// .then(res => console.log(res))



// const auctions = [
// 	{
// 		id: 1,
// 		string: 'lolo',
// 		info: {
// 			strartDate: '12/05/2018',
// 			endDate: '13/05/2018',
// 			startBid: 100,
// 			minBid: 5
// 		},
// 		lotInfo: {
// 			name: 'Huawei p7001',
// 			photo: 1
// 		}
// 	},
//
// 	{
// 		id: 2,
// 		info: {
// 			strartDate: '12/05/2018',
// 			endDate: '13/05/2018',
// 			startBid: 100,
// 			minBid: 5
// 		},
// 		lotInfo: {
// 			name: 'Huawei p7001',
// 			photo: 2
// 		}
// 	}
// ]
//
//
// const objReduce = (val, accum = {}, key) => {
// 	if (typeof val == 'object') {
// 		for (let key in val) {
// 			typeof val[key] !== 'object' ? accum[key] = val[key] : objReduce(val[key], accum,  key)
// 		}
// 	} else accum[key] = val
//
// 	return accum
// }
//
// const test = auctions.reduce((prev, next) => {
// 	prev.push(objReduce(next))
// 	return prev
// }, [])
// console.log(test);
// let categories = ['1']
// ${categories.map((cat, i) => {
// 	return i + 1 != categories.length ?
//  	'group_concat(distinct lc.CategoryId) = ' + cat + ' or' :
//  	'group_concat(distinct lc.CategoryId) = ' + cat
// }).join(' ')}

// sequelize.query(`
// select a.* \
// from auctionDev.Auctions a
// inner join auctionDev.lotCategory lc on a.LotId = lc.LotId \
// group by a.LotId, a.id \
// having group_concat(distinct lc.CategoryId) = :categories or \
// ${categories.map((cat, i) => {
// 	return i + 1 != categories.length ?
//  	'group_concat(distinct lc.CategoryId) = ' + cat + ' or' :
//  	'group_concat(distinct lc.CategoryId) = ' + cat
// }).join(' ')}`,
//   { model: Lot, type: sequelize.QueryTypes.SELECT }
// ).then(lots => {
// 	lots.forEach(lot => {
// 		console.log(lot.dataValues);
// 	})
// })

// sequelize.query("select l.* \
// 										from auctionDev.Lots l \
// 										inner join auctionDev.lotCategory lc on l.id = lc.LotId \
// 										group by lc.LotId \
// 										having group_concat(distinct lc.CategoryId) = :categories",
//
//   {  model: Lot, replacements: { categories: [1].join(',') }, type: sequelize.QueryTypes.SELECT }
// ).then(lots => {
// 	lots.forEach(lot => console.log(lot.dataValues))
// })

// Auction.findAll({
// 	include:[
// 		{model:Lot},
// 		{
// 			model: User,
// 			where: [{id:2}]
// 		}
// 	 ],
// 	 offset:1,
// 	 limit: 2,
// })
// 	.then( auctions => {
// 	let result = 	auctions
// 		.filter(auct => auct.dataValues.Lot)
// 		.map(auct => {
			// auct.dataValues.Lot = auct.dataValues.Lot.dataValues;
			// auct.dataValues.Users = auct.dataValues.Users.length;
			// return auct.dataValues;
// 		})
// 		console.log(result);
// 	})

// Auction.findById(4, {include:[{
// 		model:Lot,
// 		// where:[{id:userId}]
// 	},
// 	]})
// 	.then(auct => console.log(auct.dataValues.Lot.dataValues))
//
// let a = new Date();
// let b = new Date();
// console.log(a.getTime() == b.getTime());
// setTimeout(() => {
//   b = new Date();
// }, 1000)
let auctionId = 16;
let info ={
	auctionId: 2,
	userId: 10,
	amount: 10,
}
// Bet.create({ userId:1, auctionId: 16, amount: 20 })
// 	.then(bet => {
// 		console.log(bet.dataValues);
// 	})
//
// Bet.findAll({
// 	where: [{ auctionId }],
// })
// .then(bets => {
// 	bets.forEach(bet => console.log(bet.dataValues))
// 	console.log(bets.reduce( (amount, bet) => {
// 			return amount += bet.dataValues.amount;
// 			}, 0));
// 	console.log(bets.reduce((players, bet) => {
// 		if (players.indexOf(bet.dataValues.userId) == -1) players.push(bet.dataValues.userId);
// 		return players;
// 	}, []));
// 	console.log(bets.sort((a, b) => a.dataValues.createdAt < b.dataValues.createdAt ).map(bet => bet.dataValues));
// })
//
// Bet.create(info, {
// 		include:[{
// 			model:User
// 		}]
// })
// 	.then(res => {
// 		Bet.findById(res.dataValues.id, {
// 			include:[ { model:User }, {	model: Auction } ]
// 		})
// 			.then(bet => {
// 				console.log(bet.dataValues);
// 			})
//
// 	})
// 	.catch(err => {
// 		socket.emit('Error', {error: { code:err.code, message: err.message }});
// 	});
