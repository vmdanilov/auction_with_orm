const db = require('../models/index');
const sequelize = db.sequelize;
const User = require('../models').User
const Auction = require('../models').Auction
const Bet = require('../models').Bet
const moment = require('moment');

const secret = require("../auth_config.js");
const jwt = require("jsonwebtoken");

const ERRORS = require('./errors');

const betController = {
	getAllAuctionBets: socket => {
		Bet.findAll({
			where: [{ auctionId: socket.room }]
		})
		.then(bets => {
			socket.emit('joinRoom',
			{
				message: 'success',
				bets: bets.map(bet => bet.dataValues),
				amount: bets.reduce( (amount, bet) => {
						return amount += bet.dataValues.amount;
					}, 0),
				players: bets.reduce((players, bet) => {
					if (players.indexOf(bet.dataValues.userId) == -1) players.push(bet.dataValues.userId);
					return players;
				}, []),
				lastBet: bets.sort((a, b) => a.dataValues.createdAt < b.dataValues.createdAt )
									.map(bet => bet.dataValues)[0]
				}
			);
		})
		.catch( err => {
			socket.emit('Error', {error: { code:err.code, message: err.message }});
		})
	},

	makeBet: (io, socket, info) => {
		let { auctionId, amount } = info
		const userId = socket.handshake.query.token ? jwt.verify(
			socket.handshake.query.token,
			secret.jwtSecret
		).id : null
		User.findById(userId)
			.then(user => {
				if (user) {
					Auction.findById(auctionId)
						.then(auct => {
							let end = new Date(auct.dataValues.endDate).getTime();
							let start = new Date(auct.dataValues.startDate).getTime();

							if (end <= moment()._d.getTime() || start >= moment()._d.getTime()) {
								socket.emit('Error', {
									error: {
										message: 'auction is not active'
									}
								})
							} else {
								Bet.create({ userId, auctionId, amount })
									.then(bet => {
										io.sockets.in(socket.room).emit('changePrice', {
											message: 'success',
											bet
										});
									})
									.catch(err => {
										socket.emit('Error', {
											error: {
												code: err.code,
												message: err.message
											}
										});
									});
							}
						})
				} else {
					socket.emit('unauthorized', 'login')
				}
			})

	}

	}
module.exports = betController
