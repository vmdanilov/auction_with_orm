'use strict';
module.exports = (sequelize, DataTypes) => {
  var Bet = sequelize.define('Bet', {
    amount: {
      type: DataTypes.INTEGER,
      validate: {
        isInt: true
      }
    }
  }, {});
  Bet.associate = function(models) {
    // associations can be defined here
    models.Bet.belongsTo(models.User, {foreignKey: 'userId', allowNull: false});
    models.Bet.belongsTo(models.Auction, {foreignKey: 'auctionId', allowNull: false});
  };
  return Bet;
};
