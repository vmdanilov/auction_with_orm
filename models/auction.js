'use strict';
module.exports = (sequelize, DataTypes) => {
  var Auction = sequelize.define('Auction', {
    startDate: {
      type:DataTypes.DATE,
      validate: {
      }
    },

    endDate: {
      type:DataTypes.DATE,
      validate: {
      }
    },

    startBid: {
      type: DataTypes.INTEGER,
      validate: {
        isInt: true
      }
    },
    minStep: {
      type: DataTypes.INTEGER,
      validate: {
        isInt: true
      }
    }
  }, {});
  Auction.associate = function(models) {
    // associations can be defined here
    Auction.belongsTo(models.Lot, {onDelete: 'cascade', allowNull: false });
    Auction.belongsToMany(models.User, {through: 'userAuction'});

  };
  return Auction;
};
