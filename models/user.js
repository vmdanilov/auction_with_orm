'use strict';
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    role: {
      type: DataTypes.STRING,
      allowNull:false,
      defaultValue: 'user',
      validate: {
         isIn: [['user', 'admin']]
      }
    },
    email: {
      unique: true,
      type: DataTypes.STRING,
      allowNull:false,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull:false,
      validate: {
        is: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/g
      }

    },
    name: {
      type: DataTypes.STRING,
      allowNull:false,
      validate: {
        isAlpha: true
      }
    },
    surname: {
      type: DataTypes.STRING,
      validate: {
        isAlpha: true
      }
    },
    phone: {
      type: DataTypes.STRING,
      allowNull:false,
      // validate: {
      //   is: /^\+[1-9]{1}[0-9]{3,14}$/g
      // }
    },
    address: {
      type: DataTypes.STRING,
      // validate: {
      //   is: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/g
      // }
    }

  }, {});
  User.associate = function(models) {
    // associations can be defined here
    User.hasMany(models.Lot);
    User.belongsToMany(models.Auction, {through: 'userAuction'});
  };
  return User;
};
