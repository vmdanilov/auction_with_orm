'use strict';
module.exports = (sequelize, DataTypes) => {
  var Lot = sequelize.define('Lot', {
    name:{
      type: DataTypes.STRING,
      validate: {
        isAlpha: true,
        notEmpty: true
      }
    },
    photo: {
        type: DataTypes.STRING,
        validate: {
      }
    },
    doc:     DataTypes.STRING,
    info :   DataTypes.STRING(1234),
    UserId:  DataTypes.INTEGER,

  }, {});
  Lot.associate = function(models) {
    // associations can be defined here
    Lot.hasOne(models.Auction, { onDelete: 'cascade' });
    Lot.belongsTo(models.User, { forignKey: 'userId', onDelete: 'cascade' });
    Lot.belongsToMany(models.Category, { through: 'lotCategory' });
  };
  return Lot;
};
