'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
    let test = [];

    for (let i = 1; i < 11; i++) {
			let randVal = + (Math.random()*2 + 1).toFixed(0);
      test.push({
        LotId: i,
        CategoryId: randVal,
				createdAt: new Date(),
				updatedAt:  new Date()
			 });
    }

		return queryInterface.bulkInsert({ tableName: 'lotCategory'	}, test, {});

	},

	down: function(queryInterface, Sequelize) {
		return queryInterface.bulkDelete({
			tableName: 'lotCategory',
		}, null, {});
	}
};
