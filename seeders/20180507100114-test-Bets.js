'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
    let test = [];

    for (let i = 1; i < 11; i++) {
      test.push({
				id:i,
        amount: (Math.random()*i* 5 + 5).toFixed(0),
        userId: +(Math.random()*(i-1) + 1).toFixed(0),
        auctionId: +(Math.random()*(i-1) + 1).toFixed(0),
				createdAt: new Date(),
				updatedAt:  new Date()
			 });
    }

		return queryInterface.bulkInsert({ tableName: 'Bets'	}, test, {});

	},

	down: function(queryInterface, Sequelize) {
		return queryInterface.bulkDelete({
			tableName: 'Bets',
		}, null, {});
	}
};
