'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
    let test = [];
    let names = [null,'Switch', 'Router', 'Controller', 'Display', 'Wire', 'Ats', 'Computer', 'Laptop', 'Mobile', 'Notepad']

    for (let i = 1; i < 11; i++) {
      test.push({
				id:i,
				name: names[i],
        info: 'Lorem ipsum dolor sit amet, consectetur \
				adipisicing elit. Ratione ab, esse saepe, alias earum, \
				odit voluptate ipsum atque labore eos sed \
				adipisicing elit. Ratione ab, esse saepe, alias earum, \
				odit voluptate ipsum atque labore eos sed \
				adipisicing elit. Ratione ab, esse saepe, alias earum, \
				odit voluptate ipsum atque labore eos sed \
				adipisicing elit. Ratione ab, esse saepe, alias earum, \
				odit voluptate ipsum atque labore eos sed \
				adipisicing elit. Ratione ab, esse saepe, alias earum, \
				odit voluptate ipsum atque labore eos sed \
				adipisicing elit. Ratione ab, esse saepe, alias earum, \
				odit voluptate ipsum atque labore eos sed \
				adipisicing elit. Ratione ab, esse saepe, alias earum, \
				odit voluptate ipsum atque labore eos sed \
				adipisicing elit. Ratione ab, esse saepe, alias earum, \
				odit voluptate ipsum atque labore eos sed \
				excepturi dignissimos ex vitae.',
        UserId: i,
				createdAt: new Date(),
				updatedAt:  new Date(),
				photo: `http://localhost:8000/public/photo/${i}.jpg`,
				doc: `http://localhost:8000/public/photo/1526749606883-1_Titulny_list.docx`
			 });
    }

		return queryInterface.bulkInsert({ tableName: 'Lots'	}, test, {});

	},

	down: function(queryInterface, Sequelize) {
		return queryInterface.bulkDelete({
			tableName: 'Lots',
		}, null, {});
	}
};
