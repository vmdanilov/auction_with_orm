'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
    let test = [];
		let names = [null,'Glen', 'Rick', 'Daril', 'Dail', 'Marla', 'Tayler', 'Bob', 'Frodo', 'Gendelph', 'Sauron'];
    for (let i = 1; i < 10; i++) {
      test.push({
				id:i,
				role: 'user',
				email: names[i].toLowerCase() + '@mail.com',
				password: 'qwerty12',
				name: names[i],
				phone: '+375291231231',
				createdAt: new Date(),
				updatedAt:  new Date()
			 });
    }

		return queryInterface.bulkInsert(
      {
				tableName: 'Users'
			},

			[
        {
        id: 10,
        role: 'admin',
				email: 'vlad@vlad.com',
        password: 'qwerty12',
        name: 'Vlad',
        surname: 'Danilov',
        phone: '+37529112233441',
        createdAt: new Date(),
        updatedAt: new Date()
			  },
        ...test

      ],

      {});

	},

	down: function(queryInterface, Sequelize) {
		return queryInterface.bulkDelete({
			tableName: 'Users',
		}, null, {});
	}
};
