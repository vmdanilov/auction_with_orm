'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
    let test = [];
    let names = [null,'Switch', 'Router', 'Controller', 'Display', 'Wire', 'Ats', 'Computer', 'Laptop', 'Mobile', 'Notepad']

    for (let i = 1; i < 11; i++) {
      test.push({
				id:i,
        lotId: i,
        startBid: 100,
        minStep: 5,
				startDate: new Date(),
				endDate: new Date( new Date().setDate(new Date().getDate() + 1)),
				createdAt: new Date(),
				updatedAt:  new Date()
			 });
    }

		return queryInterface.bulkInsert({ tableName: 'Auctions'	}, test, {});

	},

	down: function(queryInterface, Sequelize) {
		return queryInterface.bulkDelete({
			tableName: 'Auctions',
		}, null, {});
	}
};
