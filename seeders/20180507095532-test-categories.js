'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
    let test = []
    test.push(
       {
         id:1,
  	     name: 'Communication',
  	     createdAt: new Date(),
  	     updatedAt: new Date()
       },
       {
         id: 2,
  	     name: 'power',
  	     createdAt: new Date(),
  	     updatedAt: new Date()
       },
       {
         id:3,
  	     name: 'electric',
  	     createdAt: new Date(),
  	     updatedAt: new Date()
       },
 );
		return queryInterface.bulkInsert( {	tableName: 'Categories' }, test, {});
	},

	down: function(queryInterface, Sequelize) {
		return queryInterface.bulkDelete({
			tableName: 'Categories',
		}, null, {});
	}
};
