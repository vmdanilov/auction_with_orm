'use strict';

module.exports = {
	up: function(queryInterface, Sequelize) {
    let test = [];

    for (let i = 1; i < 11; i++) {
			let randVal = +(Math.random()*10 + 1).toFixed(0);
      test.push({
        AuctionId: i,
        UserId: randVal,
				createdAt: new Date(),
				updatedAt:  new Date()
			 });
    }

		return queryInterface.bulkInsert({ tableName: 'userAuction'	}, test, {});

	},

	down: function(queryInterface, Sequelize) {
		return queryInterface.bulkDelete({
			tableName: 'userAuction',
		}, null, {});
	}
};
